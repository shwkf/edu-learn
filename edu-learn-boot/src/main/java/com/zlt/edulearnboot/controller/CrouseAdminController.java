package com.zlt.edulearnboot.controller;

import cn.hutool.core.util.ObjectUtil;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.bo.CourseBo;
import com.zlt.edulearnboot.entity.bo.CourseUpdateBo;
import com.zlt.edulearnboot.entity.bo.TeacherUpdateBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.CourseAdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@Slf4j
@RequestMapping("/courseAdmin")
@Api(value = "课程后台管理")
public class CrouseAdminController {

    @Autowired
    private CourseAdminService courseAdminService;

    @PostMapping("/add")
    @ApiOperation(value = "新增课程")
    public Result addCourse(@RequestBody @ApiParam(value = "课程信息") CourseBo courseBo){
        try{
            if (ObjectUtil.isNull(courseBo)) {
                throw new BaseException(400, "CourseBo为空");
            }
            return courseAdminService.addCourse(courseBo);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }


    @DeleteMapping("/deleteCourse/{id}")
    @ApiOperation("删除课程")
    public Result deleteCourse(@ApiParam(value = "课程id")@PathVariable String id) throws Exception {
        try {
            if (ObjectUtil.isNull(id)){
                throw new BaseException(400,"id为null");
            }
            return courseAdminService.deleteCourse(id);
        }catch (BaseException e) {
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }


    @PutMapping("/updateCourse")
    @ApiOperation(value = "修改课程信息")
    public Result updateCourse(@RequestBody @ApiParam(value = "课程信息") CourseUpdateBo courseUpdateBo)throws IOException {
        try {
            if (ObjectUtil.isNull(courseUpdateBo))
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            return courseAdminService.updateCourse(courseUpdateBo);
        } catch (BaseException e) {
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }


    @GetMapping("/serchCourse")
    @ApiOperation("查询课程列表")
    public CommonPage<Course> serchCourse(@ApiParam("课程名") @RequestParam(required = false) String name,
                                            @ApiParam(value = "当前页数")@RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                            @ApiParam(value = "每页几条")@RequestParam(value = "pageSize",required = false,defaultValue = "10")Integer pageSize){
        return courseAdminService.serchCourse(name,pageNum,pageSize);
    }

    @GetMapping("/selectCourse")
    @ApiOperation("查询所有课程")
    public Result selectCourse(){
        return courseAdminService.selectCourse();
    }
}
