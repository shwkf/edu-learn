package com.zlt.edulearnboot.controller;

import cn.hutool.core.util.ObjectUtil;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.View;
import com.zlt.edulearnboot.entity.bo.ViewBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.ViewAdminService;
import com.zlt.edulearnboot.utils.UploadUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/viewAdmin")
@Api(value = "面试题后台管理")
public class ViewAdminController {
    @Autowired(required = false)
    private ViewAdminService viewAdminService;

    @DeleteMapping("/deleteView")
    @ApiOperation(value = "删除面试题")
    public Result deleteView(@ApiParam(value = "面试id") @RequestParam(value = "id")String id){
        return viewAdminService.deleteView(id);
    }

    @PostMapping("/addView")
    @ApiOperation(value = "添加面试题")
    public Result addView(@ApiParam(value = "面试题对象") @RequestBody ViewBo viewBo){
        try {
            if (ObjectUtil.isNull(viewBo)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return viewAdminService.addView(viewBo);
        }catch (BaseException e){
        log.error(e.getMessage());
        return Result.Failed(e.getMessage());
        }
    }

    @PutMapping("/updateView")
    @ApiOperation(value = "修改面试题")
    public Result updateView(@RequestBody View view){
        try {
            if (ObjectUtil.isNull(view)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return viewAdminService.updateView(view);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }

    }

    @GetMapping("/selectAllView")
    @ApiOperation(value = "查询所有面试题")
    public Result selectAllView(@ApiParam(value = "当前页数")@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
                                          @ApiParam(value = "每页几条")@RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize){
        return viewAdminService.selectAllView(pageNum, pageSize);
    }

    @GetMapping("/selectOneView")
    @ApiOperation(value = "根据ID查面试题")
    public Result selectOneView(@ApiParam(value = "面试题ID")@RequestParam(value = "id") String id){
        return viewAdminService.selectOneView(id);
    }
}
