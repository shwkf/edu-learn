package com.zlt.edulearnboot.controller;

import cn.hutool.core.util.ObjectUtil;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.User;
import com.zlt.edulearnboot.entity.bo.UserAddBo;
import com.zlt.edulearnboot.entity.bo.UserUpdateBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("addUser")
    @ApiOperation(value = "新增用户")
    public Result addUser(@RequestBody @ApiParam(value = "用户信息") UserAddBo userAddBo) throws IOException {
        try {
            if (ObjectUtil.isNull(userAddBo))
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            return userService.addUser(userAddBo);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @PostMapping("updateUser")
    @ApiOperation(value = "修改用户")
    public Result updateUser(@RequestBody @ApiParam(value = "用户信息") UserUpdateBo userUpdateBo){
        try {
            if (ObjectUtil.isNull(userUpdateBo))
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            return userService.updateUser(userUpdateBo);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @PostMapping("deleteUser")
    @ApiOperation(value = "删除用户")
    public Result deleteUser(@ApiParam(value = "用户id") @PathVariable String id) throws  Exception{
        return userService.deleteUser(id);
    }

    @PostMapping("serchUser")
    @ApiOperation(value = "查询用户列表")
    public CommonPage<User> serchUser(@ApiParam(value = "用户昵称") @RequestParam(required = false) String name,
                                        @ApiParam(value = "当前页数")@RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                        @ApiParam(value = "每页几条")@RequestParam(value = "pageSize",required = false,defaultValue = "10")Integer pageSize){
        return userService.serchUser(name,pageNum,pageSize);
    }
}
