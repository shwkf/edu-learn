package com.zlt.edulearnboot.controller;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Member;
import com.zlt.edulearnboot.entity.User;
import com.zlt.edulearnboot.entity.bo.MemberAddBo;
import com.zlt.edulearnboot.entity.bo.MemberBo;
import com.zlt.edulearnboot.entity.bo.MemberUpdateBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.enums.SmsUseEnum;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.MemberService;
import com.zlt.edulearnboot.service.SmsService;
import com.zlt.edulearnboot.utils.JsonUtils;
import com.zlt.edulearnboot.utils.JwtUtils;
import com.zlt.edulearnboot.utils.UuidUtil;
import com.zlt.edulearnboot.utils.ValidatorUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static cn.hutool.log.StaticLog.info;

/**
 * <p>
 * 会员 前端控制器
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@RestController
@Slf4j
@Api(value = "会员管理")
@RequestMapping("/member")
public class MemberController {

    @Autowired
    private MemberService memberService;
    @Autowired
    private SmsService smsService;
    @Autowired
    private RedisTemplate redisTemplate;



    @GetMapping("/selectAll")
    @ApiOperation("查询所有会员")
    public Result selectAllMember(@ApiParam(value = "当前页数") @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                                  @ApiParam(value = "每页几条") @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
    ){
        return memberService.selectAllMember(pageNum,pageSize);
    }

    @PostMapping("addMember")
    @ApiOperation(value = "新增会员")
    public Result addMember(@RequestBody @ApiParam(value = "会员信息") MemberAddBo memberAddBo) throws IOException {
        try {
            if (ObjectUtil.isNull(memberAddBo))
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            return memberService.addMember(memberAddBo);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @PutMapping("updateMember")
    @ApiOperation(value = "修改会员")
    public Result updateMember(@RequestBody @ApiParam(value = "会员信息") MemberUpdateBo memberUpdateBo){
        try {
            if (ObjectUtil.isNull(memberUpdateBo))
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            return memberService.updateMember(memberUpdateBo);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @DeleteMapping("deleteMember/{id}")
    @ApiOperation(value = "删除会员")
    public Result deleteMember(@ApiParam(value = "会员id") @PathVariable String id) throws  Exception{
        return memberService.deleteMember(id);
    }

    @PostMapping("selectMember")
    @ApiOperation(value = "查询会员")
    public CommonPage<Member> serchMember(@ApiParam(value = "会员姓名") @RequestParam(required = false) String name,
                                          @ApiParam(value = "当前页数")@RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                          @ApiParam(value = "每页几条")@RequestParam(value = "pageSize",required = false,defaultValue = "10")Integer pageSize){
        return memberService.serchMember(name,pageNum,pageSize);
    }


    /**
     * 保存，id有值时更新，无值时新增
     */
    @PostMapping("/register")
    @ApiOperation(value = "注册用户")
    public Result register(@RequestBody MemberBo memberBo) {
        // 保存校验
        ValidatorUtil.require(memberBo.getMobile(), "手机号");
        ValidatorUtil.length(memberBo.getMobile(), "手机号", 11, 11);
        ValidatorUtil.require(memberBo.getPassword(), "密码");

        // 密码加密
        memberBo.setPassword(DigestUtils.md5DigestAsHex(memberBo.getPassword().getBytes()));
        return memberService.reg(memberBo);

//        // 校验短信验证码
//        SmsDto smsDto = new SmsDto();
//        smsDto.setMobile(memberDto.getMobile());
//        smsDto.setCode(memberDto.getSmsCode());
//        smsDto.setUse(SmsUseEnum.REGISTER.getCode());
//        smsService.validCode(smsDto);
//        LOG.info("短信验证码校验通过");
//
//        ResponseDto responseDto = new ResponseDto();
//        memberService.save(memberDto);
//        responseDto.setContent(memberDto);
    }

    /**
     * 登录
     */
    @PostMapping("/login")
    @ApiOperation(value = "用户登录")
    public Result login(@RequestBody MemberBo memberBo, HttpServletRequest req, HttpServletResponse resp) {
        log.info("用户登录开始");
        memberBo.setPassword(DigestUtils.md5DigestAsHex(memberBo.getPassword().getBytes()));
        Member member = memberService.selectMember(memberBo);
        if (member != null){
            String token = JwtUtils.createToken(member.getId(), member.getMobile());
            resp.setHeader("Token",token);
            redisTemplate.opsForValue().set(member.getId(),member);
            return Result.Ok(member,"登录成功");
        }else {
            return Result.Failed("登录失败");
        }

//        // 根据验证码token去获取缓存中的验证码，和用户输入的验证码是否一致
//        String imageCode = (String) redisTemplate.opsForValue().get(memberDto.getImageCodeToken());
//        log.info("从redis中获取到的验证码：{}", imageCode);
//        if (StringUtils.isEmpty(imageCode)) {
//            responseDto.setSuccess(false);
//            responseDto.setMessage("验证码已过期");
//            log.info("用户登录失败，验证码已过期");
//            return responseDto;
//        }
//        if (!imageCode.toLowerCase().equals(memberDto.getImageCode().toLowerCase())) {
//            responseDto.setSuccess(false);
//            log.("验证码不对");
//            LOG.info("用户登录失败，验证码不对");
//            return responseDto;
//        } else {
//            // 验证通过后，移除验证码
//            redisTemplate.delete(memberDto.getImageCodeToken());
//        }
//
//        LoginMemberDto loginMemberDto = memberService.login(memberDto);
//        String token = UuidUtil.getShortUuid();
//        loginMemberDto.setToken(token);
//        redisTemplate.opsForValue().set(token, JSON.toJSONString(loginMemberDto), 3600, TimeUnit.SECONDS);
//        responseDto.setContent(loginMemberDto);
//        return responseDto;
    }

    /**
     * 退出登录
     */
    @GetMapping("/logout")
    @ApiOperation(value = "退出登录")
    public Result logout(HttpServletRequest req){
        String token = req.getHeader("Token");
        //从载荷中获取uid
        String uid = JwtUtils.getMemberOftoken(token, "uid");
        //通过uid删除redis缓存中的信息
        info("从redis中删除token:{}", token);
        redisTemplate.delete(uid);
        //查看user信息是否存在存在则注销失败
        if (redisTemplate.hasKey(uid)){
            return Result.Failed(500,"注销失败");
        }else {
            return Result.Ok(200,"成功");
        }
    }

//    /**
//     * 校验手机号是否存在
//     * 存在则success=true，不存在则success=false
//     */
//    @GetMapping(value = "/is-mobile-exist/{mobile}")
//    public ResponseDto isMobileExist(@PathVariable(value = "mobile") String mobile) throws BusinessException {
//        LOG.info("查询手机号是否存在开始");
//        ResponseDto responseDto = new ResponseDto();
//        MemberDto memberDto = memberService.findByMobile(mobile);
//        if (memberDto == null) {
//            responseDto.setSuccess(false);
//        } else {
//            responseDto.setSuccess(true);
//        }
//        return responseDto;
//    }
//
//    @PostMapping("/reset-password")
//    public ResponseDto resetPassword(@RequestBody MemberDto memberDto) throws BusinessException {
//        LOG.info("会员密码重置开始:");
//        memberDto.setPassword(DigestUtils.md5DigestAsHex(memberDto.getPassword().getBytes()));
//        ResponseDto<MemberDto> responseDto = new ResponseDto();
//
//        // 校验短信验证码
//        SmsDto smsDto = new SmsDto();
//        smsDto.setMobile(memberDto.getMobile());
//        smsDto.setCode(memberDto.getSmsCode());
//        smsDto.setUse(SmsUseEnum.FORGET.getCode());
//        smsService.validCode(smsDto);
//        LOG.info("短信验证码校验通过");
//
//        // 重置密码
//        memberService.resetPassword(memberDto);
//
//        return responseDto;
//    }


}
