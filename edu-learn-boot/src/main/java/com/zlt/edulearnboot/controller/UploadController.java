package com.zlt.edulearnboot.controller;

import com.qiniu.util.Auth;
import com.zlt.edulearnboot.dto.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/upload")
public class UploadController {
    private   String accessKey = "o9Av-Y-AZqi8WSI2ZGwEjqwgbn4IK3uSVTsMtMYQ";
    private   String secretKey = "osEoyOIMbg_Z0JPyYah7Z4HWDzSR_m_M_UDaFiLx";
    private   String bucket = "education-online-1";
        /**
         * 文件上传
         * @param fileName 文件名
         * @return
         */
        @GetMapping("/upload")
        public Result getToken(@RequestParam(value = "fileName") String fileName){
            Auth auth = Auth.create(accessKey, secretKey);
            String token = auth.uploadToken(bucket);
            String key = fileName.hashCode() + UUID.randomUUID().toString() + fileName.substring(fileName.indexOf("."));
            Map<String,String> map = new HashMap<>();
            map.put("token", token);
            map.put("key", key);
            System.out.println(fileName);
            System.out.println(token);
            return Result.Ok(map, "获取成功");
    }
}
