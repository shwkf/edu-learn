package com.zlt.edulearnboot.controller;


import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Homework;
import com.zlt.edulearnboot.service.HomeworkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 作业 前端控制器
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@RestController
@RequestMapping("/homework")
@Api(value = "作业前端控制器")
public class HomeworkController {
    @Autowired
    private HomeworkService homeworkService;

    @GetMapping("/homework")
    @ApiOperation(value = "小节对应的章节")
    public Result<List<Homework>> showHomework(@ApiParam(value = "小节的id") @RequestParam(value = "sectionId") String sectionId){
        List<Homework> homework = homeworkService.showHomework(sectionId);
        Result<List<Homework>> result = new Result<>();
        result.setData(homework);
        return result;
    }
}
