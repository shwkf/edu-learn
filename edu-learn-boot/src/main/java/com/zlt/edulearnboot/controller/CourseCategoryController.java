package com.zlt.edulearnboot.controller;


import cn.hutool.core.util.StrUtil;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.bo.CourseCategoryBo;
import com.zlt.edulearnboot.entity.vo.CourseVo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.CourseCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * <p>
 * 课程分类 前端控制器
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@RestController
@RequestMapping("/course-category")
@Api(value = "课程-分类控制器")
@Slf4j
public class CourseCategoryController {

    @Autowired
    private CourseCategoryService courseCategoryService;

    @ApiOperation(value = "根据分类id查找对应课程")
    @GetMapping("/selectCourse")
    public Result selectCourseByCategoryId(@ApiParam(value = "分类id") @RequestParam(value = "categoryId", required = false) String categoryId,
                                                         @ApiParam(value = "当前页数") @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                                                         @ApiParam(value = "每页几条") @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
    ) throws IOException {
        try {
            if (StrUtil.isBlank(categoryId)) {
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return courseCategoryService.selectCourseByCategoryId(categoryId, pageNum, pageSize);
        } catch (BaseException e) {
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }

    }


    @ApiOperation(value = "新增课程分类")
    @PostMapping("/addCourseCategory")
    public Result addCourseCategory(CourseCategoryBo courseCategoryBo){
        return courseCategoryService.addCourseCategory(courseCategoryBo);
    }
}
