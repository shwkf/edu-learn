package com.zlt.edulearnboot.controller;


import cn.hutool.core.util.StrUtil;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.Member;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.MemberCourseService;
import com.zlt.edulearnboot.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * <p>
 * 会员课程报名 前端控制器
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@RestController
@Api(value = "member-course类")
@Slf4j
@RequestMapping("/member-course")
public class MemberCourseController {

    @Autowired
    private MemberCourseService memberCourseService;

    @Autowired
    private RedisTemplate redisTemplate;


    @ApiOperation(value = "课程报名")
    @PostMapping("/enrollCourse/{courseid}")
    public Result enrollCourse(@ApiParam(value = "课程id")@PathVariable("courseid") String courseid, HttpServletRequest request) throws IOException {
        try {
            if (StrUtil.isBlank(courseid)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            String token = request.getHeader("Token");
            String memberId = JwtUtils.getMemberOftoken(token, "id");
            Member member = (Member) redisTemplate.opsForValue().get(memberId);
            return memberCourseService.enrollCourse(courseid,member.getId());
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }

    }

    @ApiOperation(value = "取消报名")
    @PostMapping("/cancleCourse/{courseid}")
    public Result cancleCourse(@ApiParam(value = "课程id")@PathVariable("courseid") String courseid, HttpServletRequest request) throws IOException {
        try {
            if (StrUtil.isBlank(courseid)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            String token = request.getHeader("Token");
            String memberId = JwtUtils.getMemberOftoken(token, "id");
            Member member = (Member) redisTemplate.opsForValue().get(memberId);
            return memberCourseService.cancleCourse(courseid,member.getId());
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @ApiOperation(value = "已选课程")
    @PostMapping("selectCourses")
    public CommonPage<Course> selectCourses(@ApiParam(value = "会员昵称") @RequestParam(required = false) String id,
                                          @ApiParam(value = "当前页数")@RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                          @ApiParam(value = "每页几条")@RequestParam(value = "pageSize",required = false,defaultValue = "10")Integer pageSize){
        return memberCourseService.selectCourses(id,pageNum,pageSize);
    }
}
