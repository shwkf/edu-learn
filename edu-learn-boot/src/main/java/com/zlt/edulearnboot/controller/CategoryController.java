package com.zlt.edulearnboot.controller;


import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Category;
import com.zlt.edulearnboot.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 分类 前端控制器
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@RestController
@RequestMapping("/category")
@Api(value = "课程分类类")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @ApiOperation(value = "查询所有分类")
    @GetMapping("/all")
    public Result selectAll(){
        Result<List<Category>> result = new Result<>();
        result.setData(categoryService.sellectAll());
        return result;
    }

}
