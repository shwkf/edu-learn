package com.zlt.edulearnboot.controller;

import cn.hutool.core.util.ObjectUtil;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Homework;
import com.zlt.edulearnboot.entity.bo.HomeworkBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.HomeworkMapper;
import com.zlt.edulearnboot.service.HomeworkAdminService;
import com.zlt.edulearnboot.utils.UploadUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/HomeworkAdmin")
@Api(value = "作业管理")
public class HomeworkAdminController {

    @Autowired(required = false)
    private HomeworkAdminService homeworkAdminService;

    @DeleteMapping("/deleteHomework")
    @ApiOperation(value = "删除作业")
    public Result deleteHomework(@ApiParam(value = "作业id") @RequestParam(value = "id") String id){
        try {
            if ("".equals(id)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return homeworkAdminService.deleteHomework(id);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @PostMapping("/addHomework")
    @ApiOperation(value = "添加作业")
    @Transactional
    public Result addHomework(@ApiParam(value = "作业信息") @RequestParam(value = "homeworkBo") HomeworkBo homeworkBo,
                              @ApiParam(value = "请求") HttpServletRequest request){
        try {
            if (ObjectUtil.isNull(homeworkBo)){
                throw  new BaseException(ResultCode.VALIDATE_FAILED);
            }
            Map<String, String> map = UploadUtils.upload(homeworkBo.getName(), request.getHeader("Token"));
            Result result = homeworkAdminService.addHomework(homeworkBo,map);
            return result;
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    /**
     * 更新作业
     * @param homework
     * @return
     */
    @PutMapping("/updateHomework")
    @ApiOperation(value = "更新作业")
    public Result updateHomework(@ApiParam(value = "作业信息") @RequestParam(value = "homework") Homework homework){
        return homeworkAdminService.updateHomework(homework);
    }

    /**
     * 查询所有作业
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("/selectAllHomework")
    @ApiOperation(value = "查询所有作业")
    public Result selectAllHomework(@ApiParam(value = "当前页数")@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
                                                  @ApiParam(value = "每页几条")@RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize) {
        try {
            CommonPage<Homework> homeworkCommonPage = homeworkAdminService.selectAllHomework(pageNum, pageSize);
            if (ObjectUtil.isNull(homeworkCommonPage)){
                throw  new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return Result.Ok(homeworkCommonPage,"查询成功");
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    /**
     * 根据id查询作业
     * @param id
     * @return
     */
    @GetMapping("/selectHomework")
    @ApiOperation(value = "根据id查询作业")
    public Result selectHomework(String id){
        return homeworkAdminService.selectHomework(id);
    }
}
