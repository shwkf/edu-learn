package com.zlt.edulearnboot.controller;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Teacher;
import com.zlt.edulearnboot.entity.bo.TeacherAddBo;
import com.zlt.edulearnboot.entity.bo.TeacherUpdateBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.TeacherAdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Api(value = "讲师管理后台")
@RestController
@Slf4j
@RequestMapping("/teacherAdmin")
public class TeacherAdminController {

    //private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(TeacherAdminController.class);

    @Autowired
    private TeacherAdminService teacherAdminService;

    @PostMapping("/addteacher")
    @ApiOperation(value = "新增讲师")
    public Result addTeacher(@RequestBody @ApiParam(value = "讲师信息") TeacherAddBo teacherBo) throws IOException {
        try{
            if (ObjectUtil.isNull(teacherBo))
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            return teacherAdminService.addTeacher(teacherBo);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }

    }

    @PutMapping("/updateTeacher")
    @ApiOperation(value = "修改讲师信息")
    public Result updateTeacher(@RequestBody @ApiParam(value = "讲师信息") TeacherUpdateBo teacherBo)throws IOException{
        try {
            if (ObjectUtil.isNull(teacherBo))
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            return teacherAdminService.updateTeacher(teacherBo);
        } catch (BaseException e) {
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @DeleteMapping("/deleteTeacher/{id}")
    @ApiOperation("删除讲师")
    public Result deleteTeacher(@ApiParam(value = "讲师id")@PathVariable String id) throws Exception {
        try{
            if (ObjectUtil.isNull(id)){
                throw new BaseException(400,"传入id为空");
            }
            return teacherAdminService.deleteTeacher(id);
        }catch (BaseException e){
            log.error(e.getMsg());
            return Result.Failed(e.getMessage());
        }
    }

    @GetMapping("/serchTeacher")
    @ApiOperation("查询教师列表")
    public CommonPage<Teacher> serchTeacher(@ApiParam("教师姓名") @RequestParam(required = false) String name,
                                            @ApiParam(value = "当前页数")@RequestParam(required = false,defaultValue = "1") Integer pageNum,
                                            @ApiParam(value = "每页几条")@RequestParam(required = false,defaultValue = "10")Integer pageSize){
        if (pageNum == null){
            pageNum = 1;
        }
        if (pageSize == null){
            pageSize = 10;
        }
        return teacherAdminService.serchTeacher(name,pageNum,pageSize);
    }
}
