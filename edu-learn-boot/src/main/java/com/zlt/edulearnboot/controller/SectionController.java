package com.zlt.edulearnboot.controller;


import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Section;
import com.zlt.edulearnboot.entity.bo.SectionAdminBo;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.SectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 小节 前端控制器
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@RestController
@RequestMapping("/section")
@Slf4j
@Api(value = "小节管理")
public class SectionController {

    @Autowired
    private SectionService sectionService;

    @ApiOperation(value = "删除小节")
    @DeleteMapping("/deleteSection/{id}")
    public Result delete(@ApiParam(value = "小节id")@PathVariable String id){
        try {
            if (id != null){
                throw new BaseException("id为空");
            }
            return sectionService.delete(id);
        }catch (BaseException e){
            log.error(e.getMsg());
            return Result.Failed(e.getMsg());
        }
    }

    @ApiOperation(value = "更新小节")
    @PutMapping("/updateSection}")
    public Result update(@ApiParam(value = "小结")@RequestBody SectionAdminBo sectionAdminBo) {
        try {
            if (sectionAdminBo != null) {
                throw new BaseException("id为空");
            }
            return sectionService.updateSection(sectionAdminBo);
        } catch (BaseException e) {
            log.error(e.getMsg());
            return Result.Failed(e.getMsg());
        }
    }

    @ApiOperation(value = "增加小节")
    @PostMapping("/updateSection}")
    public Result add(@ApiParam(value = "小结")@RequestBody SectionAdminBo sectionAdminBo) {
        try {
            if (sectionAdminBo != null) {
                throw new BaseException("id为空");
            }
            return sectionService.addSection(sectionAdminBo);
        } catch (BaseException e) {
            log.error(e.getMsg());
            return Result.Failed(e.getMsg());
        }
    }


}
