package com.zlt.edulearnboot.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.bo.CategoryBo;
import com.zlt.edulearnboot.entity.vo.FirstLevelVo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.CategoryAdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.io.IOException;
import java.util.List;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/categoryAdmin")
@Api("课程分类后台")
public class CategoryAdminController {
    @Autowired
    private CategoryAdminService categoryAdminService;

    @GetMapping("/getAll")
    @ApiOperation("获取所有分类")
    public Result getAllCategory() {
        List<FirstLevelVo> list = categoryAdminService.getFirstAndSecondCategories();
        return  Result.Ok(list,"查询所有分类成功");
    }

    @GetMapping("/getFirst")
    @ApiOperation("获取所有一级分类")
    public Result getFirstLevel() {
        return categoryAdminService.selectFirstLevel();
    }

    @GetMapping("/getSecond")
    public Result getSecondLevel(){
        System.out.println("+++++++++++++++++++++"+categoryAdminService.selectSecondLevel());
        return categoryAdminService.selectSecondLevel();
    }

    @ApiOperation("删除分类")
    @DeleteMapping("/delete/{categoryId}")
    public Result deleteCategory(@ApiParam(value = "分类id")@PathVariable String categoryId) throws IOException {
        try {
            if (StrUtil.isBlank(categoryId)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return categoryAdminService.deleteCategory(categoryId);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @PostMapping("/addFirst")
    @ApiOperation(value = "新增一级分类")
    public Result addFirst(@RequestBody @ApiParam(value = "分类信息") CategoryBo categoryBo) throws IOException {
        try{
            if (ObjectUtil.isNull(categoryBo))
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            return categoryAdminService.addFirCategory(categoryBo);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }

    }

    @PostMapping("/addSec")
    @ApiOperation(value = "新增二级分类")
    public Result addSec(@RequestBody @ApiParam(value = "分类信息") CategoryBo categoryBo) throws IOException {
        try{
            if (ObjectUtil.isNull(categoryBo))
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            return categoryAdminService.addSecCategory(categoryBo);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }




}
