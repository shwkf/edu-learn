package com.zlt.edulearnboot.controller;


import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.vo.CourseReportVo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@RestController
@RequestMapping("/course")
@Slf4j
@Api(value = "课程前端控制器")
public class CourseController {
    @Autowired
    private CourseService courseService;

    @GetMapping("*")
    @ApiOperation(value = "展示推荐课程")
    public Result<List<Course>> recommend(){
        List<Course> courses = courseService.recommend();
        Result<List<Course>> result = new Result<>();
        result.setData(courses);
        return result;
    }

    @ApiOperation(value = "课程搜索接口")
    @GetMapping("/serch")
    public Result serch (@ApiParam(value = "课程名字") @RequestParam(value = "courseId",required = false)String name,
                                       @ApiParam(value = "当前页数")@RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                       @ApiParam(value = "每页几条")@RequestParam(value = "pageSize",required = false,defaultValue = "10") Integer pageSize
                                          ){
        return courseService.serch(name,pageNum,pageSize);
    }

    @ApiOperation("根据课程id查找课程")
    @GetMapping("/detailed{courseId}")
    public Result detailed(@ApiParam(value = "课程id")@PathVariable String courseId) throws IOException {
        try {
            if (StrUtil.isBlank(courseId)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return courseService.selectCourseById(courseId);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @ApiOperation(value = "导出报表信息")
    @GetMapping("/export")
    public void export(HttpServletResponse response,@ApiParam(value = "年份")@RequestParam(value = "year") String year,
                         @ApiParam(value = "月份")@RequestParam(value = "month") String month){
        try {
            //  设置内容格式 以及 编码方式
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            //   调用service去获取数据
            List<CourseReportVo> resultList= courseService.selectAll(year,month);
            //  使用java8新特性的stream流去处理数据，把空的数据过滤掉
            List<CourseReportVo> resultBo = resultList.stream().filter(Objects::nonNull)
                    .map(t -> {
                        return CourseReportVo.builder()
                                .month(t.getMonth())
                                .name(t.getName())
                                .enroll(t.getEnroll())
                                .price(t.getPrice())
                                .build();
                    }).collect(Collectors.toList());
            //  设置文件名称
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("测试导出", "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
            //  sheet名称
            EasyExcel.write(response.getOutputStream(), CourseReportVo.class).sheet("测试导出").doWrite(resultBo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/serchReport")
    @ApiOperation(value = "查询报表")
    public Result serch(@ApiParam(value = "年份")@RequestParam(value = "year") String year,
                        @ApiParam(value = "月份")@RequestParam(value = "month") String month){

        if (year == null){
            throw new BaseException(500,"年份为空");
        }
        if (month == null){
            throw new BaseException(500,"月份为空");
        }
        List<CourseReportVo> resultList= courseService.selectAll(year,month);
        return Result.Failed(resultList);
    }
}
