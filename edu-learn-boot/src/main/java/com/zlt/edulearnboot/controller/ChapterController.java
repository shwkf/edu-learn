package com.zlt.edulearnboot.controller;


import cn.hutool.core.util.StrUtil;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.bo.ChapterBo;
import com.zlt.edulearnboot.entity.bo.SectionAdminBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.ChapterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * <p>
 * 大章 前端控制器
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@RestController
@RequestMapping("/chapter")
@Api(value = "大章类")
@Slf4j
public class ChapterController {

    @Autowired
    private ChapterService chapterService;

    @ApiOperation(value = "查询大纲")
    @GetMapping("/selectChapter/{courseId}")
    public Result selectChater(@ApiParam(value ="课程id") @PathVariable String courseId) throws IOException {
        try {
            if (StrUtil.isBlank(courseId)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return chapterService.select(courseId);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @ApiOperation(value = "删除大章")
    @DeleteMapping("/deleteChapter/{id}")
    public Result delete(@ApiParam(value = "大章id")@PathVariable String id){
        try {
            if (id != null){
                throw new BaseException("id为空");
            }
            return chapterService.delete(id);
        }catch (BaseException e){
            log.error(e.getMsg());
            return Result.Failed(e.getMsg());
        }
    }

    @ApiOperation(value = "更新大纲")
    @PutMapping("/updateChapterBo}")
    public Result update(@ApiParam(value = "大章")@RequestBody ChapterBo chapterBo) {
        try {
            if (chapterBo != null) {
                throw new BaseException("id为空");
            }
            return chapterService.updateSection(chapterBo);
        } catch (BaseException e) {
            log.error(e.getMsg());
            return Result.Failed(e.getMsg());
        }
    }

    @ApiOperation(value = "增加大章")
    @PostMapping("/updateChapter}")
    public Result add(@ApiParam(value = "大章")@RequestBody ChapterBo chapterBo) {
        try {
            if (chapterBo != null) {
                throw new BaseException("id为空");
            }
            return chapterService.addSection(chapterBo);
        } catch (BaseException e) {
            log.error(e.getMsg());
            return Result.Failed(e.getMsg());
        }
    }

}
