package com.zlt.edulearnboot.controller;


import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Teacher;
import com.zlt.edulearnboot.service.TeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@RestController
@Api(value = "讲师前端控制器")
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @GetMapping("*")
    @ApiOperation(value = "金牌讲师")
    public Result<List<Teacher>> prominentTeacher(){
        List<Teacher> teacherlist = teacherService.prominentTeacher();
        Result<List<Teacher>> result = new Result<>();
        result.setData(teacherlist);
        return result;
    }
}
