package com.zlt.edulearnboot.controller;



import cn.hutool.core.util.ObjectUtil;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.core.util.StrUtil;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Member;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.entity.vo.CourseVo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.service.OrdersService;
import com.zlt.edulearnboot.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@RestController
@RequestMapping("/orders")
@Api(value = "订单")

@Slf4j
public class OrdersController {
    @Autowired
    private OrdersService ordersService;

    @Autowired
    private RedisTemplate redisTemplate;

    @ApiOperation("根据订单号查询历史订单")
    @GetMapping("/history/{ordernum}")
    public Result selectHistoricalOrder(@ApiParam(value = "课程id")@PathVariable String ordernum) throws IOException {
        try{
            if(StrUtil.isBlank(ordernum)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return ordersService.selectOrderByOrderNum(ordernum);
        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }
    }

    @ApiOperation(value = "根据用户id查找所有订单")
    @GetMapping("/selectOrdersByMemberId")
    public Result selectCourseByCategoryId(@ApiParam(value = "用户id") @RequestParam(value = "memberId", required = false) String memberId,
                                           @ApiParam(value = "当前页数") @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                                           @ApiParam(value = "每页几条") @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
    ) throws IOException {
        try {
            if (StrUtil.isBlank(memberId)) {
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return ordersService.selectAllOrders(memberId, pageNum, pageSize);
        } catch (BaseException e) {
            log.error(e.getMessage());
            return Result.Failed(e.getMessage());
        }

    }



    /**
     * 创建订单
     * @param id
     * @param req
     * @return
     */
    @ApiOperation(value = "创建订单")
    @PostMapping("/addorder")
    public Result AddOrder(@ApiParam(value = "课程id") @RequestParam(value = "id") String id, HttpServletRequest req){
        try{
            if(ObjectUtil.isNull(id)){
                throw new BaseException("id为空");
            }
            //获取token
            String token = req.getHeader("Token");
            if (JwtUtils.checkToken(token)){
                throw new BaseException(ResultCode.AUTHORIZATION_HEADER_IS_EMPTY);
            }
            //获取id
            String memberId = JwtUtils.getMemberOftoken(token, "id");
            //获取用户信息
            Member member = (Member) redisTemplate.opsForValue().get(memberId);
            return ordersService.createOrder(member, id);

        }catch (BaseException e){
            log.error(e.getMessage());
            return Result.Failed(e.getMsg());
        }

    }

}
