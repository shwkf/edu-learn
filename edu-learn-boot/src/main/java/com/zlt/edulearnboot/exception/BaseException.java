package com.zlt.edulearnboot.exception;
import com.zlt.edulearnboot.enums.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BaseException extends RuntimeException{

    private ResultCode resultCode;

    private long code;

    private String msg;


    public BaseException(String msg){
        super(msg);
    }
    public BaseException(String msg,Throwable cause){
        super(msg,cause);
    }

    /**
     * 自定义异常信息
     * @param code
     * @param msg
     */
    public BaseException(long code,String msg){
        this.code = code;
        this.msg = msg;
    }

    /**
     * 通过resultCode设置异常消息
     * @param resultCode
     */
    public BaseException(ResultCode resultCode){
        this.resultCode = resultCode;
    }



}
