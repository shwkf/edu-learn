package com.zlt.edulearnboot.config;


import com.zlt.edulearnboot.interceptor.AuthInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {


    @Bean
    public AuthInterceptor authInterceptor() {
        return new AuthInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 放行路径
//        List<String> patterns = new ArrayList();
//        patterns.add("/member/selectAll");
//        patterns.add("/member/addMember");
//        patterns.add("/member/updateMember");
//        patterns.add("/member/deleteMember");
//        patterns.add("/member/register");
//        patterns.add("/member/login");
//        patterns.add("/druid/**");
//        registry.addInterceptor(authInterceptor()).addPathPatterns("/**")
//                .excludePathPatterns(patterns)
//                .excludePathPatterns("/swagger-resources/**"
//                 ,"/webjars/**"
//                 ,"/v2/**"
//                 ,"/swagger-ui.html/**");
    }
}