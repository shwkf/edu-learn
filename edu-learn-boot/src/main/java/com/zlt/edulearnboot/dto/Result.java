package com.zlt.edulearnboot.dto;

import com.zlt.edulearnboot.enums.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//FIXEME 需要进行对回包数据进行整改

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {

    private  long code;

    private  String msg;

    private  T data;

    /**
     * 返回错误结果(无数据)
     * @param <T>
     * @return
     */
    public static<T> Result<T> Failed(){
        return new Result<T>(ResultCode.FAILED.getCode(),ResultCode.FAILED.getMsg(),null);
    }

    /**
     * 返回错误结果(有数据)
     * @param data
     * @param <T>
     * @return
     */
    public static<T> Result<T> Failed(T data){
        return new Result<T>(ResultCode.FAILED.getCode(),ResultCode.FAILED.getMsg(),data);
    }

    /**
     * 返回错误结果自定义消息
     * @param data
     * @param msg
     * @param <T>
     * @return
     */

    public static<T> Result<T> Failed(T data,String msg){
        return new Result<T>(ResultCode.FAILED.getCode(),msg,data);
    }
    /**
     * 返回成功结果(无数据)
     * @param <T>
     * @return
     */
    public static<T> Result<T> Ok(){
        return new Result<T>(ResultCode.OK.getCode(),ResultCode.OK.getMsg(),null);
    }

    /**
     * 返回成功结果(有数据)
     * @param data
     * @param <T>
     * @return
     */
    public static<T> Result<T> Ok(T data){
        return new Result<T>(ResultCode.OK.getCode(),ResultCode.OK.getMsg(),data);
    }

    /**
     * 返回错误结果自定义数据
     * @param data
     * @param msg
     * @param <T>
     * @return
     */
    public static<T> Result<T> Ok(T data,String msg){
        return new Result<T>(ResultCode.OK.getCode(),msg,data);
    }

}
