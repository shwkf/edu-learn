package com.zlt.edulearnboot.utils;

public class StringUtils {

    /**
     * 获取一个随机字符串
     * @param len
     * @return
     */
    public static String getRandomString(int len){

        String s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0;i<len;i++){
            //随机获取字符串s中的一个字符
            char c = s.charAt((int) (Math.random() * s.length()));
            //每次获取的字符合并起来
            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }
}
