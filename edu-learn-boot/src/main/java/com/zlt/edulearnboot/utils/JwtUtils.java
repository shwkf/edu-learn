package com.zlt.edulearnboot.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @ClassName JwtUtils
 * @Author KingLee
 * @date 2020.08.20 14:25
 */
public class JwtUtils {

    private static long exp = 1000 * 60 * 60;//过期时间为一个小时

    private static String security = "Zxcv@lZCDVJK23bnadv431sasbnm/3ertyu,i5678";//密钥

    /**
     * 生成token
     *
     * @param id
     * @param mobile
     * @return
     */

    public static String createToken(String id, String mobile) {
        Map header = new HashMap();
        header.put("typ", "JWT");
        header.put("alg", "HS256");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis() + exp);

        return JWT.create().
                withHeader(header).//头
                withIssuedAt(new Date()).//发布时间
                withExpiresAt(calendar.getTime()).//过期时间
                withSubject("*"). //面向的用户
                withClaim("id", id).
                withClaim("mobile", mobile).
                sign(Algorithm.HMAC256(security));//加密

    }


    /**
     * 检查token是否存在和有效
     *
     * @return
     */
    public static boolean checkToken(String token) {

        if (token == null) {
            System.out.println("token为null");
            return false;
        }
        try {
            //创建验证对象
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(security)).build();
            //拿出存进去的信息
            DecodedJWT decodedJWT = verifier.verify(token);
        } catch (JWTVerificationException e) {
            return false;
        }
        return true;
    }

    /**
     * 从指定的token中获取指定的载荷
     *
     * @param token
     * @param key
     * @return
     */
    public static String getMemberOftoken(String token, String key) {

        if (!checkToken(token)) {
            System.out.println("token异常");
            return null;
        }
        DecodedJWT decodedJWT = null;
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(security)).build();
            decodedJWT = verifier.verify(token);

        } catch (JWTVerificationException e) {
            e.printStackTrace();
        }
        return decodedJWT.getClaim(key).asString();
    }


}
