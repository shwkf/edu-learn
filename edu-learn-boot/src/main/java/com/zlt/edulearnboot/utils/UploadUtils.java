package com.zlt.edulearnboot.utils;

import com.qiniu.util.Auth;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UploadUtils {
    private static final String accessKey = "o9Av-Y-AZqi8WSI2ZGwEjqwgbn4IK3uSVTsMtMYQ";
    private static final String secretKey = "osEoyOIMbg_Z0JPyYah7Z4HWDzSR_m_M_UDaFiLx";
    private static final String bucket = "education-online-1";

    /**
     * 文件上传
     * @param fileName 文件名
     * @param token 七牛生成的token
     * @return
     */
    public static Map<String ,String> upload(String fileName,String token){
        //判断token是否为空
        if (token==null) {
            Auth auth = Auth.create(accessKey, secretKey);
            token = auth.uploadToken(bucket);
        }
        String key = fileName.hashCode() + UUID.randomUUID().toString() + fileName.substring(fileName.indexOf("."));
        Map<String,String> map = new HashMap<>();
        map.put("token", token);
        map.put("key", key);
        return map;
    }
}
