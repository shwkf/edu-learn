package com.zlt.edulearnboot.entity.bo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ViewBo implements Serializable {
    private String fileName;
    private Integer size;
    private String courseId;
    private String path;
}
