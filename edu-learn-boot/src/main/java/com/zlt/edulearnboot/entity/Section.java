package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 小节
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("section")
@ApiModel(value="Section对象", description="小节")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Section implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "标题")
    @TableField("title")
    private String title;

    @ApiModelProperty(value = "课程|course.id")
    @TableField("course_id")
    private String courseId;

    @ApiModelProperty(value = "大章|chapter.id")
    @TableField("chapter_id")
    private String chapterId;

    @ApiModelProperty(value = "视频")
    @TableField("video")
    private String video;

    @ApiModelProperty(value = "时长|单位秒")
    @TableField("time")
    private Integer time;

    @ApiModelProperty(value = "收费|C 收费；F 免费")
    @TableField("charge")
    private String charge;

    @ApiModelProperty(value = "顺序")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty(value = "0未删除，1删除")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "vod|阿里云vod")
    @TableField("vod")
    private String vod;

}
