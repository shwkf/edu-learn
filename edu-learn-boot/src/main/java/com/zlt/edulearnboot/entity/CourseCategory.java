package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 课程分类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("course_category")
@ApiModel(value="CourseCategory对象", description="课程分类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "课程|course.id")
    @TableField("course_id")
    private String courseId;

    @ApiModelProperty(value = "分类|course.id")
    @TableField("category_id")
    private String categoryId;

}
