package com.zlt.edulearnboot.entity.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "分类信息")
public class CategoryBo implements Serializable {

    @ApiModelProperty(value = "父id")
    @TableField("parent")
    private String parent;

    @ApiModelProperty(value = "名称")
    @TableField("name")
    private String name;
}
