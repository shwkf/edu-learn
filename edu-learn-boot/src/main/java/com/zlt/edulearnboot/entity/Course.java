package com.zlt.edulearnboot.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 课程
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("course")
@ApiModel(value="Course对象", description="课程")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "概述")
    @TableField("summary")
    private String summary;

    @ApiModelProperty(value = "时长|单位秒")
    @TableField("time")
    private Integer time;

    @ApiModelProperty(value = "价格（元）")
    @TableField("price")
    private BigDecimal price;

    @ApiModelProperty(value = "封面")
    @TableField("image")
    private String image;

    @ApiModelProperty(value = "级别|枚举[CourseLevelEnum]：ONE('1' , '初级'),TWO('2', '中级'),THREE('3', '高级')")
    @TableField("level")
    private String level;

    @ApiModelProperty(value = "收费|枚举[CourseChargeEnum]：CHARGE('C', '收费'),FREE('F', '免费')")
    @TableField("charge")
    private String charge;

    @ApiModelProperty(value = "状态|枚举[CourseStatusEnum]：PUBLISH('P', '发布'),DRAFT('D', '草稿')")
    @TableField("status")
    private String status;

    @ApiModelProperty(value = "报名数")
    @TableField("enroll")
    private Integer enroll;

    @ApiModelProperty(value = "顺序")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty(value = "0未删除，1删除")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "讲师|teacher.id")
    @TableField("teacher_id")
    private String teacherId;

}
