package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 角色用户关联
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("role_user")
@ApiModel(value="RoleUser对象", description="角色用户关联")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "角色|id")
    @TableField("role_id")
    private String roleId;

    @ApiModelProperty(value = "用户|id")
    @TableField("user_id")
    private String userId;

}
