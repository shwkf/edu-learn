package com.zlt.edulearnboot.entity.vo;

import lombok.Data;

import java.util.List;


@Data
public class ChapterVo {

    private String id;

    private String name;

    private List<SectionVo> sectionVoList;

}
