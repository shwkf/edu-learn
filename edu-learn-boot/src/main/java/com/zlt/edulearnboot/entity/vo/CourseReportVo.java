package com.zlt.edulearnboot.entity.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder


public class CourseReportVo {

    @ExcelProperty(value = "月份")
    private String month;

    @ExcelProperty(value = "课程名")
    private String name;

    @ExcelProperty(value = "报名人数")
    private  Integer enroll;

    @ExcelProperty(value = "课程收益")
    private BigDecimal price;



}
