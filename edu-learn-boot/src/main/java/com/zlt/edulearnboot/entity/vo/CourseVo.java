package com.zlt.edulearnboot.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@ApiModel("课程实体类")
@Data
public class CourseVo {

    @ApiModelProperty("id")
    @TableId("id")
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty("课程名")
    @TableField("name")
    private String name;

    /**
     * 概述
     */
    @ApiModelProperty("概述")
    @TableField("summary")
    private String summary;


    /**
     * 价格（元）
     */
    @ApiModelProperty("课程价格id")
    @TableField("price")
    private BigDecimal price;

    /**
     * 封面
     */
    @ApiModelProperty("课程封面")
    @TableField("image")
    private String image;


    /**
     * 状态|枚举[CourseStatusEnum]：PUBLISH("P", "发布"),DRAFT("D", "草稿")
     */
    @ApiModelProperty("课程状态")
    @TableField("status")
    private String status;

    /**
     * 报名数
     */
    @ApiModelProperty("报名数")
    @TableField("enroll")
    private Integer enroll;

    /**
     * 0未删除，1删除
     */
    @ApiModelProperty("课程是否删除")
    @TableField("is_delete")
    private Integer isDelete;


    /**
     * 老师de姓名
     */
    @ApiModelProperty("教师名字")
    @TableField("name")
    private String teacherName;

}
