package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 测试
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("test")
@ApiModel(value="Test对象", description="测试")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Test implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "名称")
    @TableField("name")
    private String name;

}
