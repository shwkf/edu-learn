package com.zlt.edulearnboot.entity.vo;

import lombok.Data;

@Data
public class HomeWorkVo {

    private String id;

    private String name;

    private String path;
}
