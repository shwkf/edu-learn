package com.zlt.edulearnboot.entity.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "课程信息")
public class CourseBo implements Serializable {


    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "名称")
    @TableField("name")
    @NotBlank
    private String name;

    @ApiModelProperty(value = "概述")
    @TableField("summary")
    private String summary;

    @ApiModelProperty(value = "时长|单位秒")
    @TableField("time")
    private Integer time;

    @ApiModelProperty(value = "价格（元）")
    @TableField("price")
    private BigDecimal price;

    @ApiModelProperty(value = "封面")
    @TableField("image")
    private String image;

    @ApiModelProperty(value = "级别|枚举[CourseLevelEnum]：ONE('1' , '初级'),TWO('2', '中级'),THREE('3', '高级')")
    @TableField("level")
    private String level;

    @ApiModelProperty(value = "收费|枚举[CourseChargeEnum]：CHARGE('C', '收费'),FREE('F', '免费')")
    @TableField("charge")
    private String charge;

    @ApiModelProperty(value = "状态|枚举[CourseStatusEnum]：PUBLISH('P', '发布'),DRAFT('D', '草稿')")
    @TableField("status")
    private String status;


    @ApiModelProperty(value = "顺序")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty(value = "0未删除，1删除")
    @TableField("is_delete")
    private Integer isDelete;

}
