package com.zlt.edulearnboot.entity.bo;

import lombok.Data;

import java.io.Serializable;
@Data
public class HomeworkBo implements Serializable {
    private String name;
    private Integer size;
    private String section_id;
}
