package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 会员课程报名
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("member_course")
@ApiModel(value="MemberCourse对象", description="会员课程报名")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberCourse implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "会员id")
    @TableField("member_id")
    private String memberId;

    @ApiModelProperty(value = "课程id")
    @TableField("course_id")
    private String courseId;

    @ApiModelProperty(value = "报名时间")
    @TableField("at")
    private LocalDateTime at;

    @ApiModelProperty(value = "1删除，0未删除")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;

}
