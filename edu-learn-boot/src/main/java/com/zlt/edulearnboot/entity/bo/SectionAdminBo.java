package com.zlt.edulearnboot.entity.bo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@TableName("section")
@ApiModel(value="更新小节")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SectionAdminBo {

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "标题")
    @TableField("title")
    private String title;

    @ApiModelProperty(value = "课程|course.id")
    @TableField("course_id")
    private String courseId;

    @ApiModelProperty(value = "大章|chapter.id")
    @TableField("chapter_id")
    private String chapterId;

    @ApiModelProperty(value = "视频")
    @TableField("video")
    private String video;


    @ApiModelProperty(value = "收费|C 收费；F 免费")
    @TableField("charge")
    private String charge;


    @ApiModelProperty(value = "0未删除，1删除")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    private LocalDateTime updateTime;


}
