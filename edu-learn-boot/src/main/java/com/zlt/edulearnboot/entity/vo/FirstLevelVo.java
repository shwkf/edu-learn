package com.zlt.edulearnboot.entity.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

//一级分类课程
@Data
public class FirstLevelVo {
    private String id;
    private String name;
    private List<SecondLevelVo> children = new ArrayList<>();
}