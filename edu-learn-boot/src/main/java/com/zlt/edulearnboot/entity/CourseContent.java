package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 课程内容
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("course_content")
@ApiModel(value="CourseContent对象", description="课程内容")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseContent implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "课程内容")
    @TableField("content")
    private String content;

}
