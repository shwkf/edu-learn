package com.zlt.edulearnboot.entity.vo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ViewVo {
    private String id;
    private String path;
    private String name;
    private String suffix;
    private Integer size;
    private String courseId;
    private String courseName;
    private LocalDateTime updateTime;
}
