package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 文件
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@TableName("file")
@ApiModel(value="File对象", description="文件")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class File implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "相对路径")
    @TableField("path")
    private String path;

    @ApiModelProperty(value = "文件名")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "后缀")
    @TableField("suffix")
    private String suffix;

    @ApiModelProperty(value = "大小|字节B")
    @TableField("size")
    private Integer size;

    @ApiModelProperty(value = "用途|枚举[FileUseEnum]：COURSE('C', '讲师'), TEACHER('T', '课程')")
    @TableField("use")
    private String use;

    @ApiModelProperty(value = "0未删除，1删除")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "已上传分片")
    @TableField("shard_index")
    private Integer shardIndex;

    @ApiModelProperty(value = "分片大小|B")
    @TableField("shard_size")
    private Integer shardSize;

    @ApiModelProperty(value = "分片总数")
    @TableField("shard_total")
    private Integer shardTotal;

    @ApiModelProperty(value = "文件标识")
    @TableField("key")
    private String key;

    @ApiModelProperty(value = "vod|阿里云vod")
    @TableField("vod")
    private String vod;
}
