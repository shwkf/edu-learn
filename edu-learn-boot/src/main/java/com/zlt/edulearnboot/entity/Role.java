package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("role")
@ApiModel(value="Role对象", description="角色")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "角色")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "描述")
    @TableField("desc")
    private String desc;

}
