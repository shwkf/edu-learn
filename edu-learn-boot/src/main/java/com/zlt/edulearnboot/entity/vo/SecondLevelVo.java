package com.zlt.edulearnboot.entity.vo;

import lombok.Data;

//二级分类课程
@Data
public class SecondLevelVo {
    private String id;
    private String name;
}
