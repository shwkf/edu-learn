package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 资源
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("resource")
@ApiModel(value="Resource对象", description="资源")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Resource implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "名称|菜单或按钮")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "页面|路由")
    @TableField("page")
    private String page;

    @ApiModelProperty(value = "请求|接口")
    @TableField("request")
    private String request;

    @ApiModelProperty(value = "父id")
    @TableField("parent")
    private String parent;

}
