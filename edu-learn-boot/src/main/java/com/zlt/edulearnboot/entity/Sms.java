package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 短信验证码
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@TableName("sms")
@ApiModel(value="Sms对象", description="短信验证码")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Sms implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "手机号")
    @TableField("mobile")
    private String mobile;

    @ApiModelProperty(value = "验证码")
    @TableField("code")
    private String code;

    @ApiModelProperty(value = "用途|枚举[SmsUseEnum]：REGISTER('R', '注册'), FORGET('F', '忘记密码')")
    @TableField("use")
    private String use;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "用途|枚举[SmsStatusEnum]：USED('U', '已使用'), NOT_USED('N', '未使用')")
    @TableField("status")
    private String status;

}
