package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("homework")
@ApiModel(value="Homework对象", description="作业")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Homework implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "相对路径")
    @TableField("path")
    private String path;

    @ApiModelProperty(value = "文件名")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "后缀")
    @TableField("suffix")
    private String suffix;

    @ApiModelProperty(value = "大小|字节B")
    @TableField("size")
    private Integer size;

    @ApiModelProperty(value = "章节")
    @TableField("section_id")
    private String sectionId;

    @ApiModelProperty(value = "1删除，0未删除")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

}
