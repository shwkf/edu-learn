package com.zlt.edulearnboot.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zlt.edulearnboot.entity.Course;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel("订单")
@Data
public class OrderVo implements Serializable {

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "业务订单号")
    @TableField("order_number")
    private String orderNumber;

    @ApiModelProperty(value = "产品快照")
    @TableField("order_info")
    private String orderInfo;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "课程")
    private Course course;
}
