package com.zlt.edulearnboot.entity.vo;

import lombok.Data;

import java.util.List;

@Data
public class SectionVo {

    private String id;

    private String title;

    private String video;

    private List<HomeWorkVo> homeWorkVoList;
}
