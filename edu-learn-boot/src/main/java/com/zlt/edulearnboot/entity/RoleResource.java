package com.zlt.edulearnboot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 角色资源关联
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@TableName("role_resource")
@ApiModel(value="RoleResource对象", description="角色资源关联")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleResource implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "角色|id")
    @TableField("role_id")
    private String roleId;

    @ApiModelProperty(value = "资源|id")
    @TableField("resource_id")
    private String resourceId;

}
