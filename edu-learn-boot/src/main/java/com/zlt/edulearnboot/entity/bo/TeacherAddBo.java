package com.zlt.edulearnboot.entity.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.sun.istack.internal.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel(value = "新增讲师类")
public class TeacherAddBo implements Serializable {

    @ApiModelProperty(value = "姓名")
    @TableField("name")
    @NotBlank
    private String name;

    @ApiModelProperty(value = "昵称")
    @TableField("nickname")
    private String nickname;

    @ApiModelProperty(value = "头像")
    @TableField("image")
    private String image;

    @ApiModelProperty(value = "职位")
    @TableField("position")
    private String position;

    @ApiModelProperty(value = "座右铭")
    @TableField("motto")
    private String motto;

    @ApiModelProperty(value = "简介")
    @TableField("intro")
    private String intro;
}
