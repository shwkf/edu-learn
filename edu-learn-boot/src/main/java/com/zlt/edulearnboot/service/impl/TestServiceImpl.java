package com.zlt.edulearnboot.service.impl;

import com.zlt.edulearnboot.entity.Test;
import com.zlt.edulearnboot.mapper.TestMapper;
import com.zlt.edulearnboot.service.TestService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测试 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements TestService {

}
