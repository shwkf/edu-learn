package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.Sms;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 短信验证码 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface SmsService extends IService<Sms> {

}
