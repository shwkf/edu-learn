package com.zlt.edulearnboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Member;
import com.zlt.edulearnboot.entity.bo.MemberAddBo;
import com.zlt.edulearnboot.entity.bo.MemberBo;
import com.zlt.edulearnboot.entity.bo.MemberUpdateBo;

/**
 * <p>
 * 会员 服务类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
public interface MemberService extends IService<Member> {

    Result addMember(MemberAddBo memberAddBo);

    Result updateMember(MemberUpdateBo memberUpdateBo);

    Result deleteMember(String id) throws  Exception;

    CommonPage<Member> serchMember(String name, Integer pageNum, Integer pageSize);

    Result reg(MemberBo memberBo);

    Member selectMember(MemberBo memberBo);

    Result selectAllMember(Integer pageNum, Integer pageSize);
}
