package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Chapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zlt.edulearnboot.entity.bo.ChapterBo;

/**
 * <p>
 * 大章 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface ChapterService extends IService<Chapter> {

    //查询大纲
    Result select(String courseId);

    Result delete(String id);

    Result addSection(ChapterBo chapterBo);

    Result updateSection(ChapterBo chapterBo);
}
