package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.MemberCourse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员课程报名 服务类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
public interface MemberCourseService extends IService<MemberCourse> {

    //课程报名
    Result enrollCourse(String courseId,String memberId);

    //取消课程报名
    Result cancleCourse(String courseId,String memberId);

    //统计报名人数
    Integer countEnroll(String courseid);

    //更新报名人数
    boolean updateEnroll(String courseid,Integer enroll);

    //已选课程
    CommonPage<Course> selectCourses(String id, Integer pageNum, Integer pageSize);
}
