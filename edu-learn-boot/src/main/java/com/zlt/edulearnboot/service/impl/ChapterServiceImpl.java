package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Chapter;
import com.zlt.edulearnboot.entity.Homework;
import com.zlt.edulearnboot.entity.Section;
import com.zlt.edulearnboot.entity.bo.ChapterBo;
import com.zlt.edulearnboot.entity.vo.ChapterVo;
import com.zlt.edulearnboot.entity.vo.HomeWorkVo;
import com.zlt.edulearnboot.entity.vo.SectionVo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.ChapterMapper;
import com.zlt.edulearnboot.mapper.HomeworkMapper;
import com.zlt.edulearnboot.mapper.SectionMapper;
import com.zlt.edulearnboot.service.ChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlt.edulearnboot.utils.UuidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 大章 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class ChapterServiceImpl extends ServiceImpl<ChapterMapper, Chapter> implements ChapterService {

    @Autowired
    private ChapterMapper chapterMapper;

    @Autowired
    private SectionMapper sectionMapper;

    @Autowired
    private HomeworkMapper homeworkMapper;


    @Override
    public Result select(String courseId) {
        List<ChapterVo> resultList = new ArrayList<>();
        try {
            if (StrUtil.isBlank(courseId)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            System.out.println(courseId);
            //根据课程id查找对应大章
            List<Chapter> chapters = chapterMapper.selectList(new QueryWrapper<Chapter>().eq("course_id", courseId));
            List<String> chapterId = new ArrayList<>();
            if(!chapters.isEmpty()){
                for (Chapter chapter : chapters) {
                    ChapterVo chapterVo = new ChapterVo();
                    chapterVo.setName(chapter.getName());
                    chapterVo.setId(chapter.getId());
                    chapterId.add(chapter.getId());
                    resultList.add(chapterVo);
                }

                for (ChapterVo chapterVo : resultList) {
                    List<SectionVo> sectionVoList = new ArrayList<>();
                    List<Section> sections = sectionMapper.selectList(new QueryWrapper<Section>().eq("chapter_id", chapterVo.getId()));
                    for (Section section : sections) {
                        SectionVo sectionVo = new SectionVo();
                        BeanUtil.copyProperties(section, sectionVo);
                        System.out.println(sectionVo);
                        //根据对应小节id找到对应作业
                        List<Homework> homeworkList = homeworkMapper.selectList(new QueryWrapper<Homework>().eq("section_id", section.getId()));
                        System.out.println(homeworkList);
                        List<HomeWorkVo> homeWorkVoList = new ArrayList<>();
                        for (Homework homework : homeworkList) {
                            HomeWorkVo homeworkVo = new HomeWorkVo();
                            BeanUtil.copyProperties(homework, homeworkVo);
                            homeWorkVoList.add(homeworkVo);
                        }
                        System.out.println(homeWorkVoList);
                        sectionVo.setHomeWorkVoList(homeWorkVoList);
                        sectionVoList.add(sectionVo);
                    }
                    System.out.println(sectionVoList);
                    chapterVo.setSectionVoList(sectionVoList);
                }

                System.out.println(resultList);
                    //根据大章id、课程id查询对应章节
                return Result.Ok(resultList,"查询成功");
            }else {
                return Result.Failed("查询大章失败，为空");
            }
        }catch (BaseException e){
            throw e;
        }
    }

    @Override
    public Result delete(String id) {
        if (id == null){
            throw  new BaseException("id为空");
        }
        int id1 = chapterMapper.delete(new QueryWrapper<Chapter>().eq("id", id));
        if (id1 != 1){
            return Result.Failed("删除失败");
        }else{
            return Result.Ok("删除成功");
        }
    }

    @Override
    public Result addSection(ChapterBo chapterBo) {
        Chapter chapter = new Chapter();
        if (chapterBo == null) {
            throw new BaseException(500, "参数上传错误sectionAdminBo为空");
        }
        BeanUtils.copyProperties(chapterBo, chapter);
        chapter.setId(UuidUtil.getShortUuid());
        int i = chapterMapper.insert(chapter);
        if (i != 1) {
            return Result.Failed("添加失败");
        } else {
            return Result.Ok("添加成功");
        }

    }

    @Override
    public Result updateSection(ChapterBo chapterBo) {
        Chapter chapter = new Chapter();
        if (chapterBo == null) {
            throw new BaseException(500, "参数上传错误sectionAdminBo为空");
        }
        BeanUtils.copyProperties(chapterBo, chapter);
        int i = chapterMapper.updateById(chapter);
        if (i != 1) {
            return Result.Failed("更新失败");
        } else {
            return Result.Ok("更新成功");
        }
    }
}
