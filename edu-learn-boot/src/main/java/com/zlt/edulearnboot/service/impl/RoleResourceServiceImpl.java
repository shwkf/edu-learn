package com.zlt.edulearnboot.service.impl;

import com.zlt.edulearnboot.entity.RoleResource;
import com.zlt.edulearnboot.mapper.RoleResourceMapper;
import com.zlt.edulearnboot.service.RoleResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色资源关联 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class RoleResourceServiceImpl extends ServiceImpl<RoleResourceMapper, RoleResource> implements RoleResourceService {

}
