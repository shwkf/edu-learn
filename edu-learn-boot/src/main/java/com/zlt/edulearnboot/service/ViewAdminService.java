package com.zlt.edulearnboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.View;
import com.zlt.edulearnboot.entity.bo.ViewBo;

public interface ViewAdminService extends IService<View> {
    Result deleteView(String id);
    Result addView(ViewBo viewBo);

    Result updateView(View view);

    Result selectAllView(Integer pageNum, Integer pageSize);

    Result selectOneView(String id);
}
