package com.zlt.edulearnboot.service.impl;

import com.zlt.edulearnboot.entity.CourseContentFile;
import com.zlt.edulearnboot.mapper.CourseContentFileMapper;
import com.zlt.edulearnboot.service.CourseContentFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程内容文件 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class CourseContentFileServiceImpl extends ServiceImpl<CourseContentFileMapper, CourseContentFile> implements CourseContentFileService {

}
