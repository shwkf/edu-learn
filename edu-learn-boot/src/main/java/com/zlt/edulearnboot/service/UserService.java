package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zlt.edulearnboot.entity.bo.UserAddBo;
import com.zlt.edulearnboot.entity.bo.UserUpdateBo;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
public interface UserService extends IService<User> {

    Result addUser(UserAddBo userAddBo);

    Result updateUser(UserUpdateBo userUpdateBo);

    Result deleteUser(String id);

    CommonPage<User> serchUser(String name, Integer pageNum, Integer pageSize);
}
