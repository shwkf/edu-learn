package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.User;
import com.zlt.edulearnboot.entity.bo.UserAddBo;
import com.zlt.edulearnboot.entity.bo.UserUpdateBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.UserMapper;
import com.zlt.edulearnboot.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlt.edulearnboot.utils.UuidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private UserMapper userMapper;

    @Transactional
    @Override
    public Result addUser(UserAddBo userAddBo) {
        User user = new User();
        try {
            if (userAddBo == null){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            BeanUtils.copyProperties(userAddBo, user);
            user.setId(UuidUtil.getShortUuid());
            int insert = userMapper.insert(user);
            if(insert != 1){
                return Result.Failed("新增失败");
            }else {
                return Result.Ok("新增成功");
            }
        }catch (BaseException e){
            throw e;
        }
    }

    @Transactional
    @Override
    public Result updateUser(UserUpdateBo userUpdateBo) {
        User user1 = new User();
        try {
            if (ObjectUtil.isNull(userUpdateBo.getId())){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            User user = userMapper.selectById(userUpdateBo.getId());
            if (ObjectUtil.isNull(user)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            BeanUtils.copyProperties(userUpdateBo, user1);
            int update = userMapper.updateById(user1);
            if (update != 1){
                return Result.Failed("更新失败");
            }else {
                return Result.Ok("更新成功");
            }
        }catch (BaseException e){
            throw e;
        }
    }

    @Transactional
    @Override
    public Result deleteUser(String id) {
        Result result = new Result();
        User user = userMapper.selectById(id);
        if (ObjectUtil.isNull(user)){
            throw new BaseException("会员不存在");
        }
        int i = userMapper.deleteById(id);
        if (i != 1){
            return Result.Failed("删除失败");
        }else {
            return Result.Ok("删除成功");
        }
    }

    @Override
    public CommonPage<User> serchUser(String name, Integer pageNum, Integer pageSize) {
        Page<User> userPage = userMapper.selectPage(new Page<User>(pageNum,pageSize), new QueryWrapper<User>()
                .like(name != null,"name",name));
        ArrayList<User> memberArrayList = new ArrayList<>();
        for (User record : userPage.getRecords()) {
            memberArrayList.add(record);
        }
        return CommonPage.restPage(
                memberArrayList,
                pageNum,
                pageSize,
                (int)userPage.getPages(),
                userPage.getTotal()
        );
    }
}
