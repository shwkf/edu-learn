package com.zlt.edulearnboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlt.edulearnboot.entity.CourseCategory;
import com.zlt.edulearnboot.entity.Member;
import com.zlt.edulearnboot.entity.MemberCourse;
import com.zlt.edulearnboot.entity.View;
import com.zlt.edulearnboot.mapper.*;
import com.zlt.edulearnboot.service.ViewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@Service
public class ViewServiceImpl extends ServiceImpl<ViewMapper, View> implements ViewService {
    @Autowired
    private ViewMapper viewMapper;

    @Override
    public List<View> showView(String courseId) {
        List<View> views = viewMapper.selectList(new QueryWrapper<View>().eq("course_id", courseId).eq("is_delete", 0));
        return views;
    }
}
