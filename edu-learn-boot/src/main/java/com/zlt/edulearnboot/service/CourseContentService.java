package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.CourseContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程内容 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface CourseContentService extends IService<CourseContent> {

}
