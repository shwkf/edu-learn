package com.zlt.edulearnboot.service.impl;

import com.zlt.edulearnboot.entity.Resource;
import com.zlt.edulearnboot.mapper.ResourceMapper;
import com.zlt.edulearnboot.service.ResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资源 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements ResourceService {

}
