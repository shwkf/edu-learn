package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Teacher;
import com.zlt.edulearnboot.entity.bo.TeacherAddBo;
import com.zlt.edulearnboot.entity.bo.TeacherUpdateBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.TeacherMapper;
import com.zlt.edulearnboot.service.TeacherAdminService;
import com.zlt.edulearnboot.utils.UuidUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class TeacherAdminServiceImpl implements TeacherAdminService {

    @Autowired(required = false)
    private TeacherMapper teacherMapper;

    /**
     * 新增教师
     * @param teacherBo
     * @return
     */
    @Override
    @Transactional
    public Result addTeacher(TeacherAddBo teacherBo) {
        //创建老师对象
        Teacher teacher = new Teacher();
        System.err.println(teacherBo);
        //将bo复制到对象中
        try {
            if (teacherBo == null) {
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            BeanUtils.copyProperties(teacherBo, teacher);
            teacher.setId(UuidUtil.getShortUuid());
            //新增
            int insert = teacherMapper.insert(teacher);
            if (insert != 1) {
                return Result.Failed("新增失败");
            }else {
                return Result.Ok("新增成功");
            }
        }catch (BaseException e){
            throw e;
        }
    }

    /**
     * 修改教师
     * @param teacherBo
     * @return
     */
    @Override
    @Transactional
    public Result updateTeacher(TeacherUpdateBo teacherBo) {
        //先判断讲师是否存在
        Teacher teacher = new Teacher();
        try {
            if (ObjectUtil.isNull(teacherBo.getId())) {
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            Teacher teacher1 = teacherMapper.selectById(teacherBo.getId());
            if (ObjectUtil.isNull(teacher1)) {
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            BeanUtils.copyProperties(teacherBo, teacher);
            int update = teacherMapper.updateById(teacher);
            if (update != 1) {
                return Result.Failed("更新失败");
            } else {
                return Result.Ok("更新成功");
            }
        } catch (BaseException e) {
            throw e;
        }

    }

    @Override
    @Transactional
    public Result deleteTeacher(String id){
        try{
            if (ObjectUtil.isNull(id)){
                throw new BaseException(400,"id为null");
            }
            Teacher teacher = teacherMapper.selectById(id);
            if (ObjectUtil.isNull(teacher)){
                throw new BaseException(400,"id上传错误，没有相对应的老师");
            }
            int i = teacherMapper.deleteById(id);
            if (i != 1){
                return Result.Failed("删除失败");
            }else{
                return Result.Ok("删除成功");

            }

        }catch (BaseException e){
            throw e;
        }
    }

    @Override
    @Transactional
    public CommonPage<Teacher> serchTeacher(String name, Integer pageNum, Integer pageSize) {
        Page<Teacher> teacherPage = teacherMapper.selectPage(new Page<>(pageNum, pageSize), new QueryWrapper<Teacher>()
                .like(name != null, "name", name));
        ArrayList<Teacher> resultList = new ArrayList<>();
        //循环分页查询内容
        for (Teacher record : teacherPage.getRecords()) {
            resultList.add(record);
        }
        return CommonPage.restPage(
                resultList,
                pageNum,
                pageSize,
                (int) teacherPage.getPages(),
                teacherPage.getTotal()
        );
    }
}
