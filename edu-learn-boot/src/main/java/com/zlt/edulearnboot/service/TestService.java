package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.Test;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 测试 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface TestService extends IService<Test> {

}
