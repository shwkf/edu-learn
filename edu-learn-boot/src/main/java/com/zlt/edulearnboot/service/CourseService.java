package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zlt.edulearnboot.entity.vo.CourseReportVo;
import com.zlt.edulearnboot.entity.vo.CourseVo;

import java.util.List;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
public interface CourseService extends IService<Course> {

    List<Course> recommend();

    Result serch(String name, Integer pageNum, Integer pageSize);

    Result selectCourseById(String id);


    List<CourseReportVo> selectAll(String year,String month);
}
