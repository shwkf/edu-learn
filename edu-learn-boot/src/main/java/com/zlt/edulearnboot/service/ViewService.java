package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.View;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
public interface ViewService extends IService<View> {

    List<View> showView(String memberId);
}
