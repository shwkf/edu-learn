package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.CourseCategory;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zlt.edulearnboot.entity.bo.CourseCategoryBo;

/**
 * <p>
 * 课程分类 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface CourseCategoryService extends IService<CourseCategory> {

    //根据分类id查课程
    Result selectCourseByCategoryId(String categoryId, Integer pageNum, Integer pageSize);


    Result addCourseCategory(CourseCategoryBo courseCategoryBo);
}
