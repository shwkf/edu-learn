package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.bo.CategoryBo;
import com.zlt.edulearnboot.entity.vo.FirstLevelVo;
import java.util.List;

public interface CategoryAdminService {

    List<FirstLevelVo> getFirstAndSecondCategories();

    //删除分类
    Result deleteCategory(String categoryId);

    //添加一级分类
    Result addFirCategory(CategoryBo categoryBo);

    //添加二级分类
    Result addSecCategory(CategoryBo categoryBo);

    //查找一级分类
    Result selectFirstLevel();

    Result selectSecondLevel();
}
