package com.zlt.edulearnboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlt.edulearnboot.entity.Teacher;
import com.zlt.edulearnboot.mapper.TeacherMapper;
import com.zlt.edulearnboot.service.TeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService {

    @Autowired
    private TeacherMapper teacherMapper;

    @Override
    public List<Teacher> prominentTeacher() {
        List<Teacher> teachers = teacherMapper.selectList(new QueryWrapper<Teacher>().ne("id", ""));
        return teachers;
    }
}
