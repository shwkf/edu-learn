package com.zlt.edulearnboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlt.edulearnboot.entity.Homework;
import com.zlt.edulearnboot.mapper.HomeworkMapper;
import com.zlt.edulearnboot.service.HomeworkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@Service
public class HomeworkServiceImpl extends ServiceImpl<HomeworkMapper, Homework> implements HomeworkService {

    @Autowired
    private HomeworkMapper homeworkMapper;
    @Override
    public List<Homework> showHomework(String sectionId) {
        QueryWrapper<Homework> homeworkQueryWrapper = new QueryWrapper<>();
        QueryWrapper<Homework> section_id = homeworkQueryWrapper.eq("section_id", sectionId);
        List<Homework> homeworks = homeworkMapper.selectList(section_id);
        for (Homework homework : homeworks) {
            System.out.println(homework);
        }
        return homeworks;
    }
}
