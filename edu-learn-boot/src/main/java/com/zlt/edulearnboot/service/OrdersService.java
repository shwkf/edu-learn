package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Member;
import com.zlt.edulearnboot.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface OrdersService extends IService<Orders> {

    //根据订单号查询历史订单
    Result selectOrderByOrderNum(String orderNum);

    Result createOrder(Member member, String id);
    //查询用户历史订单
    Result selectAllOrders(String id,Integer pageNum, Integer pageSize);

}
