package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Section;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zlt.edulearnboot.entity.bo.SectionAdminBo;

/**
 * <p>
 * 小节 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface SectionService extends IService<Section> {

    Result delete(String id);

    Result updateSection(SectionAdminBo sectionAdminBo);

    Result addSection(SectionAdminBo sectionAdminBo);
}
