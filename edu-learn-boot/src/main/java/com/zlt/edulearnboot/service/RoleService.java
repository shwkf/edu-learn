package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface RoleService extends IService<Role> {

}
