package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.File;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文件 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface FileService extends IService<File> {

}
