package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface TeacherService extends IService<Teacher> {

     List<Teacher> prominentTeacher();
}
