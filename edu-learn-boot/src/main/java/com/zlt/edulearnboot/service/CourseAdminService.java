package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.Teacher;
import com.zlt.edulearnboot.entity.bo.CourseBo;

public interface CourseAdminService {
    Result addCourse(CourseBo courseBo);

    Result deleteCourse(String id);

    CommonPage<Course> serchCourse(String name, Integer pageNum, Integer pageSize);

    Result selectCourse();
}
