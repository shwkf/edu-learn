package com.zlt.edulearnboot.service.impl;

import com.alibaba.fastjson.JSON;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.Member;
import com.zlt.edulearnboot.entity.Orders;
import com.zlt.edulearnboot.entity.vo.CourseInfoVo;
import com.zlt.edulearnboot.entity.vo.OrderVo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.CourseMapper;
import com.zlt.edulearnboot.mapper.MemberMapper;
import com.zlt.edulearnboot.mapper.OrdersMapper;
import com.zlt.edulearnboot.service.OrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlt.edulearnboot.utils.NumberUtils;
import com.zlt.edulearnboot.utils.UuidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {

    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    MemberMapper memberMapper;

    @Override
    public Result selectOrderByOrderNum(String orderNum) {
        try{
            if (StrUtil.isBlank(orderNum)){
                throw  new BaseException(ResultCode.VALIDATE_FAILED);
            }
            Result result = new Result();
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("order_number",orderNum);
            List<Orders> list = ordersMapper.selectList(queryWrapper);
            for (Orders s : list) {
                result.setData(s.getOrderInfo());
            }
            return result;
        }catch (BaseException e){
            throw e;
        }
    }

    @Override
    public Result selectAllOrders(String id,Integer pageNum, Integer pageSize) {
        OrderVo orderVo = new OrderVo();
        List<OrderVo> resultList = new ArrayList<>();
        try {
            if(StrUtil.isBlank(id)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            if (ObjectUtil.isNotNull(memberMapper.selectById(id))){
                Page<Orders> ordersPage = ordersMapper.selectPage(new Page<>(pageNum, pageSize), new QueryWrapper<Orders>().
                        eq("user_id",id));
                for (Orders record : ordersPage.getRecords()) {
                    BeanUtil.copyProperties(record, orderVo );
                    String courseId = record.getCourseId();
                    Course course = courseMapper.selectById(courseId);
                    orderVo.setCourse(course);
                    resultList.add(orderVo);
                }
                CommonPage<OrderVo> courseVoCommonPage = CommonPage.restPage(
                        resultList,
                        pageNum,
                        pageSize,
                        (int) ordersPage.getPages(),
                        ordersPage.getTotal()
                );
                return Result.Ok(courseVoCommonPage,"查询成功");
            }else {
                return Result.Ok("查询失败，用户不存在");
            }
        }catch (BaseException e){
            throw e;
        }
    }

    /**
     * 创建订单
     * @param member
     * @param id
     * @return
     */
    @Override
    @Transactional
    public Result createOrder(Member member, String id) {
        Orders orders = new Orders();
        CourseInfoVo vo = new CourseInfoVo();
        //封装快照内容
        Course course = courseMapper.selectById(id);
        if (course == null){
            return Result.Failed("课程id为空无法找到对应课程");
        }
        BeanUtils.copyProperties(course,vo);
        //封装orders对象
        String s = JSON.toJSONString(vo);
        orders.setId(UuidUtil.getShortUuid());
        orders.setCourseId(id);
        orders.setOrderInfo(s);
        orders.setMemberId(member.getId());
        orders.setOrderNumber(NumberUtils.generateOrderSn());
        int insert = ordersMapper.insert(orders);
        if (insert == 1){
           return Result.Ok(orders,"创建订单成功");
        }else {
            return Result.Failed("创建订单失败");
        }
    }
}
