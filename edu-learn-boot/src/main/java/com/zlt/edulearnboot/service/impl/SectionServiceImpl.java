package com.zlt.edulearnboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Section;
import com.zlt.edulearnboot.entity.bo.SectionAdminBo;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.SectionMapper;
import com.zlt.edulearnboot.service.SectionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlt.edulearnboot.utils.UuidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 小节 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class SectionServiceImpl extends ServiceImpl<SectionMapper, Section> implements SectionService {

    @Autowired
    private SectionMapper sectionMapper;

    @Override
    @Transactional
    public Result delete(String id) {
         if (id == null){
             throw  new BaseException("id为空");
         }
         int id1 = sectionMapper.delete(new QueryWrapper<Section>().eq("id", id));
         if (id1 != 1){
             return Result.Failed("删除失败");
         }else{
             return Result.Ok("删除成功");
         }
    }

    @Override
    @Transactional
    public Result updateSection(SectionAdminBo sectionAdminBo) {
        Section section = new Section();
        if (sectionAdminBo == null) {
            throw new BaseException(500, "参数上传错误sectionAdminBo为空");
        }
        String lastvideo = sectionAdminBo.getVideo();
        //切割字符暗藏
        String[] split = lastvideo.split("/");
        String newVideo = "rtmp://192.168.2.197:1935/oflaDemo/" + split;
        BeanUtils.copyProperties(sectionAdminBo, section);
        section.setVideo(newVideo);
        int i = sectionMapper.updateById(section);
        if (i != 1) {
            return Result.Failed("更新失败");
        } else {
            return Result.Ok("更新成功");
        }
    }

    @Override
    public Result addSection(SectionAdminBo sectionAdminBo) {
        Section section = new Section();
        if (sectionAdminBo == null) {
            throw new BaseException(500, "参数上传错误sectionAdminBo为空");
        }
        String lastvideo = sectionAdminBo.getVideo();
        //切割字符暗藏
        String[] split = lastvideo.split("/");
        String newVideo = "rtmp://192.168.2.197:1935/oflaDemo/" + split;
        BeanUtils.copyProperties(sectionAdminBo, section);
        section.setId(UuidUtil.getShortUuid());
        section.setVideo(newVideo);
        int i = sectionMapper.insert(section);
        if (i != 1) {
            return Result.Failed("新增失败");
        } else {
            return Result.Ok("新增成功");
        }
    }

}
