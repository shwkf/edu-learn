package com.zlt.edulearnboot.service.impl;

import com.zlt.edulearnboot.entity.RoleUser;
import com.zlt.edulearnboot.mapper.RoleUserMapper;
import com.zlt.edulearnboot.service.RoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色用户关联 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class RoleUserServiceImpl extends ServiceImpl<RoleUserMapper, RoleUser> implements RoleUserService {

}
