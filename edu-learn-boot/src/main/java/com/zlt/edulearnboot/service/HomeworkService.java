package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.Homework;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
public interface HomeworkService extends IService<Homework> {

    List<Homework> showHomework(String sectionId);
}
