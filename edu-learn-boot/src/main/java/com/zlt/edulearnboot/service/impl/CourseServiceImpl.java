package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.Teacher;
import com.zlt.edulearnboot.entity.vo.CourseReportVo;
import com.zlt.edulearnboot.entity.vo.CourseVo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.CourseMapper;
import com.zlt.edulearnboot.mapper.TeacherMapper;
import com.zlt.edulearnboot.service.CourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {

    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private TeacherMapper teacherMapper;
    @Override
    public List<Course> recommend() {
        List<Course> courses = courseMapper.selectList(new QueryWrapper<Course>().eq("is_delete",0).last("limit 10"));
        return courses;
    }


    @Override
    public Result serch(String name, Integer pageNum, Integer pageSize) {
        //查询课程
        Page<Course> course = courseMapper.selectPage(new Page<Course>(pageNum, pageSize), new QueryWrapper<Course>()
                .like(name != null, "name", name));
        //创建一个返回集合
        ArrayList<CourseVo> resultList = new ArrayList<>();
        //创建一个课程id集合
        ArrayList<String> ids = new ArrayList<>();
        //创建一个教师id的集合
        ArrayList<String> teacherId = new ArrayList<>();

        if(course.hasNext()){
            //将课程id放入集合
            for (Course record : course.getRecords()) {
                System.out.println(record.toString());
                String id = record.getId();
                String teacherIds = record.getTeacherId();
                ids.add(id);
                teacherId.add(teacherIds);
            }
            if (!teacherId.isEmpty()){
                //查询教师名字
                List<Teacher> teachers = teacherMapper.selectList(new QueryWrapper<Teacher>().in("id", teacherId));
                //创建一个teacherMap
                HashMap<String, String> teacherMap = new HashMap<>();
                for (Teacher teacher : teachers) {
                    teacherMap.put(teacher.getId(),teacher.getName());
                }
                for (Course record : course.getRecords()) {
                    CourseVo courseVo = new CourseVo();
                    BeanUtils.copyProperties(record,courseVo);
                    courseVo.setTeacherName(teacherMap.get(record.getTeacherId()));
                    resultList.add(courseVo);
                }
                CommonPage<CourseVo> courseVoCommonPage = CommonPage.restPage(
                        resultList,
                        pageNum,
                        pageSize,
                        (int) course.getPages(),
                        course.getTotal()
                );
                return Result.Ok(courseVoCommonPage,"查询成功");
            }else {
                return Result.Failed("查询失败");
            }

        }else {
            return Result.Failed("没有相关课程");
        }
    }

    @Override
    public Result selectCourseById(String id) {
        CourseVo courseVo = new CourseVo();
        try {
            if (StrUtil.isBlank(id)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            Course course = courseMapper.selectById(id);
            if (ObjectUtil.isNotNull(course)){
                if (course.getIsDelete().equals(0)){
                    String teacherId = course.getTeacherId();
                    Teacher teacher = teacherMapper.selectById(teacherId);
                    courseVo.setTeacherName(teacher.getName());
                    BeanUtil.copyProperties(course, courseVo);
                    return Result.Ok(courseVo,"查询成功");
                }else {
                    return Result.Failed("没有该课程");
                }
            }else {
                return Result.Failed("没有该课程");
            }
        }catch (BaseException e){
            throw e;
        }
    }

    @Override
    public List<CourseReportVo> selectAll(String year, String month){
        List<CourseReportVo> courseReportVos = courseMapper.selectReport(year,month);
        for (CourseReportVo courseReportVo : courseReportVos) {
            System.out.println(courseReportVo.toString());
        }
        return courseReportVos;
    }

//    @Override
//    public List<CourseReportVo> selectAll(Integer year,Integer month){
//        CourseReportVo courseReportVo = new CourseReportVo();
//        Integer people = null;
//        Integer lastYear = year;
//        Integer lastMonth = month - 1;
//        //创建一个存课程名字的集合
//        ArrayList<String> courseName = new ArrayList<>();
//        //通过报名人数降序查询这个月的课程信息(这里试试能不能selectMap<name，Course>)
//        List<Course> courses = courseMapper.selectList(new QueryWrapper<Course>().in("name",courseName)
//                .like("update_time", year + "-" + month).orderByDesc("enroll"));
//        if (courses == null){
//            throw new BaseException("当前月份并没有查询到信息");
//        }
//        for (Course cours : courses) {
//            //放入课程名
//            courseName.add(cours.getName());
//        }
//        //课程名去重
//        courseName.stream().distinct().collect(Collectors.toList());
//
//        //通过报名人数降序查询这个上个月的课程信息
//        if (month == 1){
//            lastMonth = 12;
//            lastYear = year - 1;
//        }
//        List<Course> lastMonthCourse = courseMapper.selectList(new QueryWrapper<Course>()
//                .like("update_time", lastYear + "-" + lastMonth).orderByDesc("enroll"));
//        //获取上个月的报名数
//        if (lastMonthCourse == null){
//            people = 0;
//        }else{
//            people = lastMonthCourse.get(0).getEnroll();
//        }
//        //获取这个月的报名数
//        Course course = courses.get(0);
//        //计算这个月报名数
//        Integer addPeople = course.getEnroll() - people;
//        courseMapper.selectMaps()
//        //计算这个月获利
//
//        return null;
//    }

}
