package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Teacher;
import com.zlt.edulearnboot.entity.bo.TeacherAddBo;
import com.zlt.edulearnboot.entity.bo.TeacherUpdateBo;
import io.swagger.models.auth.In;

public interface TeacherAdminService {
    Result addTeacher(TeacherAddBo teacherBo);

    Result updateTeacher(TeacherUpdateBo teacherBo);

    Result deleteTeacher(String id);

    CommonPage<Teacher> serchTeacher(String name, Integer pageNum,Integer pageSize);
}
