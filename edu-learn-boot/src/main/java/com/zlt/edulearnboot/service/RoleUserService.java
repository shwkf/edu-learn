package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.RoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色用户关联 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface RoleUserService extends IService<RoleUser> {

}
