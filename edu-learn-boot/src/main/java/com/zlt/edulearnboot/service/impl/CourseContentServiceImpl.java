package com.zlt.edulearnboot.service.impl;

import com.zlt.edulearnboot.entity.CourseContent;
import com.zlt.edulearnboot.mapper.CourseContentMapper;
import com.zlt.edulearnboot.service.CourseContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程内容 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class CourseContentServiceImpl extends ServiceImpl<CourseContentMapper, CourseContent> implements CourseContentService {

}
