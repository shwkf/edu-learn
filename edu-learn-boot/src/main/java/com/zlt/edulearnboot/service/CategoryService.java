package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 分类 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface CategoryService extends IService<Category> {

    //查询所有分类
    List<Category> sellectAll();
}
