package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.CourseCategory;
import com.zlt.edulearnboot.entity.Teacher;
import com.zlt.edulearnboot.entity.bo.CourseCategoryBo;
import com.zlt.edulearnboot.entity.vo.CourseVo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.CourseCategoryMapper;
import com.zlt.edulearnboot.mapper.CourseMapper;
import com.zlt.edulearnboot.mapper.TeacherMapper;
import com.zlt.edulearnboot.mapper.TestMapper;
import com.zlt.edulearnboot.service.CourseCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlt.edulearnboot.utils.UuidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 课程分类 服务实现类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@Service
public class CourseCategoryServiceImpl extends ServiceImpl<CourseCategoryMapper, CourseCategory> implements CourseCategoryService {

    @Autowired
    private CourseCategoryMapper courseCategoryMapper;

    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private TeacherMapper teacherMapper;

    @Override
    public Result selectCourseByCategoryId(String categoryId, Integer pageNum, Integer pageSize) {
        try{
            if (StrUtil.isBlank(categoryId)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            //创建一个返回集合
            ArrayList<CourseVo> resultList = new ArrayList<>();
            //创建一个课程id集合
            ArrayList<String> courseids = new ArrayList<>();
            //根据分类id找到课程id
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq(categoryId!=null,"category_id", categoryId);
            List<CourseCategory> courseCategories = courseCategoryMapper.selectList(queryWrapper);
            if (!courseCategories.isEmpty()){
                for (CourseCategory cc  : courseCategories){
                    courseids.add(cc.getCourseId());
                }
                //根据课程id找到对应课程
                Page<Course> course = courseMapper.selectPage(new Page<>(pageNum, pageSize), new QueryWrapper<Course>().
                        in("id",courseids).ne("is_delete", 1));
                //找到课程对应的老师
                List<String> teacherId = new ArrayList<>();
                for (Course record : course.getRecords()) {
                    System.out.println(record.toString());
                    String id = record.getId();
                    String teacherIds = record.getTeacherId();
                    teacherId.add(teacherIds);
                }
                //创建一个teacherMap
                HashMap<String, String> teacherMap = new HashMap<>();
                if (teacherId.size() != 0 ){
                    //查询教师名字
                    System.out.println(teacherId);
                    List<Teacher> teachers = teacherMapper.selectList(new QueryWrapper<Teacher>().in("id", teacherId));
                    for (Teacher teacher : teachers) {
                        teacherMap.put(teacher.getId(),teacher.getName());
                    }
                }

                for (Course record : course.getRecords()) {
                    CourseVo courseVo = new CourseVo();
                    BeanUtils.copyProperties(record,courseVo);
                    courseVo.setTeacherName(teacherMap.get(record.getTeacherId()));
                    resultList.add(courseVo);
                }
                CommonPage<CourseVo> courseVoCommonPage = CommonPage.restPage(
                        resultList,
                        pageNum,
                        pageSize,
                        (int) course.getPages(),
                        course.getTotal()
                );
                return Result.Ok(courseVoCommonPage,"查询成功");
            }else {
                return Result.Failed("没有该分类");
            }
        }catch (BaseException e){
            throw e;
        }

    }

    @Override
    public Result addCourseCategory(CourseCategoryBo courseCategoryBo) {

        CourseCategory courseCategory = new CourseCategory();
        try {
            if (ObjectUtil.isNull(courseCategoryBo)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            BeanUtil.copyProperties(courseCategoryBo,courseCategory);
            courseCategory.setId(UuidUtil.getShortUuid());
            int insert = courseCategoryMapper.insert(courseCategory);
            if (insert != 1) {
                return Result.Failed("新增失败");
            }else {
                return Result.Ok("新增成功");
            }
        }catch (BaseException e){
            throw e;
        }
    }


}
