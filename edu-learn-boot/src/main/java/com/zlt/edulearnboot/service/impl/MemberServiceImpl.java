package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.jwt.JWT;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Member;
import com.zlt.edulearnboot.entity.bo.MemberAddBo;
import com.zlt.edulearnboot.entity.bo.MemberBo;
import com.zlt.edulearnboot.entity.bo.MemberUpdateBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.MemberMapper;
import com.zlt.edulearnboot.service.MemberService;
import com.zlt.edulearnboot.utils.JwtUtils;
import com.zlt.edulearnboot.utils.StringUtils;
import com.zlt.edulearnboot.utils.UuidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 会员 服务实现类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {


    @Autowired(required = false)
    private MemberMapper memberMapper;


    @Transactional
    @Override
    public Result addMember(MemberAddBo memberAddBo) {
        Member member = memberMapper.selectOne(new QueryWrapper<Member>().eq("mobile", memberAddBo.getMobile()));
        System.out.println(memberAddBo);
        try {
            if (memberAddBo == null){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            if (member != null){
                member.setIsDelete(0);
                int i = memberMapper.updateById(member);
                if(i != 1){
                    return Result.Failed("新增失败");
                }else {
                    return Result.Ok("新增成功");
                }
            }else {
                Member member1 = new Member();
                BeanUtils.copyProperties(memberAddBo, member1);
                System.out.println(member1);
                member1.setId(UuidUtil.getShortUuid());
                int insert = memberMapper.insert(member1);
                if (insert != 1) {
                    return Result.Failed("新增失败");
                } else {
                    return Result.Ok("新增成功");
                }
            }
        }catch (BaseException e){
            throw e;
        }
    }

    @Transactional
    @Override
    public Result updateMember(MemberUpdateBo memberUpdateBo){
        Member member1 = new Member();
        try {
            if (ObjectUtil.isNull(memberUpdateBo.getId())){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            Member member = memberMapper.selectById(memberUpdateBo.getId());
            if (ObjectUtil.isNull(member)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            BeanUtils.copyProperties(memberUpdateBo, member1);
            int update = memberMapper.updateById(member1);
            if (update != 1){
                return Result.Failed("更新失败");
            }else {
                return Result.Ok("更新成功");
            }
        }catch (BaseException e){
            throw e;
        }
    }

    @Transactional
    @Override
    public Result deleteMember(String id) throws Exception {
        Result result = new Result();
        Member member = memberMapper.selectById(id);
        if (ObjectUtil.isNull(member)){
            throw new BaseException("会员不存在");
        }
        int i = memberMapper.deleteById(id);
        if (i != 1){
            return Result.Failed("删除失败");
        }else {
            return Result.Ok("删除成功");
        }
    }

    @Override
    public CommonPage<Member> serchMember(String name, Integer pageNum, Integer pageSize) {
        Page<Member> memberPage = memberMapper.selectPage(new Page<Member>(pageNum,pageSize), new QueryWrapper<Member>()
                .like(name != null,"name",name));
        ArrayList<Member> memberArrayList = new ArrayList<>();
        for (Member record : memberPage.getRecords()) {
            memberArrayList.add(record);
        }
        return CommonPage.restPage(
                memberArrayList,
                pageNum,
                pageSize,
                (int)memberPage.getPages(),
                memberPage.getTotal()
        );
    }

    @Override
    @Transactional
    public Result reg(MemberBo memberBo) {
        Member member = new Member();
        if (ObjectUtil.isNull(memberBo)){
            throw new BaseException("MemberBo为null");
        }
        //判断手机号是否存在
        Member member1 = memberMapper.selectOne(new QueryWrapper<Member>().eq("mobile", memberBo.getMobile()));
        if (member1 != null){
            Result.Failed(500,"账号已被注册");
        }
        BeanUtils.copyProperties(memberBo,member);
        member.setName(StringUtils.getRandomString(6));
        member.setPhoto("http://qvsyj51eh.hn-bkt.clouddn.com/default.jpg");
        int insert = memberMapper.insert(member);
        if (insert != 1){
            return Result.Failed(500,"注册失败");
        }else {
            return Result.Ok(ResultCode.OK);
        }
    }

    @Override
    public Member selectMember(MemberBo memberBo) {
        Member member = memberMapper.selectOne(new QueryWrapper<Member>().eq("mobile", memberBo.getMobile())
                .eq("password", memberBo.getPassword()));
        if (member != null){
           return member ;
        }else {
            return null;
        }
    }

    @Override
    public Result selectAllMember(Integer pageNum, Integer pageSize) {
        Page<Member> memberPage = memberMapper.selectPage(new Page<>(pageNum, pageSize), null);
        List<Member> resultList = new ArrayList<>();
        for (Member record : memberPage.getRecords()) {
            resultList.add(record);
        }
        CommonPage<Member> commonPage = CommonPage.restPage(
                resultList,
                pageNum,
                pageSize,
                (int) memberPage.getPages(),
                memberPage.getTotal()
        );
        return Result.Ok(commonPage,"查询成功");
    }


}

