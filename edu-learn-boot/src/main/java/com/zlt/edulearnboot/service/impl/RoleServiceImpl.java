package com.zlt.edulearnboot.service.impl;

import com.zlt.edulearnboot.entity.Role;
import com.zlt.edulearnboot.mapper.RoleMapper;
import com.zlt.edulearnboot.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
