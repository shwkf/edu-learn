package com.zlt.edulearnboot.service.impl;

import com.zlt.edulearnboot.entity.Sms;
import com.zlt.edulearnboot.mapper.SmsMapper;
import com.zlt.edulearnboot.service.SmsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 短信验证码 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class SmsServiceImpl extends ServiceImpl<SmsMapper, Sms> implements SmsService {

}
