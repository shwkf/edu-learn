package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.bo.CourseBo;
import com.zlt.edulearnboot.entity.bo.CourseUpdateBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.CourseMapper;
import com.zlt.edulearnboot.service.CourseAdminService;
import com.zlt.edulearnboot.utils.UuidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseAdminServiceImpl implements CourseAdminService {

    @Autowired
    private CourseMapper courseMapper;

    @Override
    @Transactional
    /**
     * id存在跟新
     * id不存在插入
     */
    public Result addCourse(CourseBo courseBo) {
        Course course = new Course();
        try{
            if (ObjectUtil.isNull(courseBo)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            BeanUtils.copyProperties(courseBo, course);
            if (ObjectUtil.isNull(courseBo.getId())) {
                return insert(course);
            } else {
                return update(course);
            }
        } catch (BaseException e) {
            throw e;
        }
    }

    @Override
    @Transactional
    public Result deleteCourse(String id) {
        Course course = courseMapper.selectById(id);
        if (ObjectUtil.isNull(course)){
            throw new BaseException(400,"课程id上传错误无法查找该课程");
        }
        int delete = courseMapper.deleteById(id);
        if (delete != 1){
            return Result.Failed("删除失败");
        }else {
            return Result.Ok("删除成功");

        }
    }


    @Override
    public Result updateCourse(CourseUpdateBo courseUpdateBo) {
        Course course = new Course();
        try {
            if (ObjectUtil.isNull(courseUpdateBo.getId())) {
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            Course course1 = courseMapper.selectById(courseUpdateBo.getId());
            if (ObjectUtil.isNull(course1)) {
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            BeanUtils.copyProperties(courseUpdateBo, course);
            int update = courseMapper.updateById(course);
            if (update != 1) {
                return Result.Failed("更新失败");
            } else {
                return Result.Ok("更新成功");
            }
        } catch (BaseException e) {
            throw e;
        }
    }

    @Override
    public CommonPage<Course> serchCourse(String name, Integer pageNum, Integer pageSize) {
        {
            Page<Course> coursePage = courseMapper.selectPage(new Page<Course>(pageNum, pageSize), new QueryWrapper<Course>()
                    .like(name != null, "name", name));
            ArrayList<Course> resultList = new ArrayList<>();
            //循环分页查询内容
            for (Course record : coursePage.getRecords()) {
                resultList.add(record);
            }
            return CommonPage.restPage(
                    resultList,
                    pageNum,
                    pageSize,
                    (int)coursePage.getPages(),
                    coursePage.getTotal()
            );
        }
    }

    @Override
    public Result selectCourse() {
        List<Course> courses = courseMapper.selectList(new QueryWrapper<Course>());
        return Result.Ok(courses,"查询成功");
    }


    //查询所有课程
    @Override
    public Result selectAllCourse(Integer pageNum, Integer pageSize) {
        Page<Course> coursePage = courseMapper.selectPage(new Page<>(pageNum, pageSize), null);
        List<Course> resultList = new ArrayList<>();
        for (Course record : coursePage.getRecords()) {
            resultList.add(record);
        }
        CommonPage<Course> commonPage = CommonPage.restPage(
                resultList,
                pageNum,
                pageSize,
                (int) coursePage.getPages(),
                coursePage.getTotal()
        );
        return Result.Ok(commonPage,"查询成功");
    }


    /**
     * 新增
     */
    private Result insert(Course course) {
        course.setId(UuidUtil.getShortUuid());
        int insert = courseMapper.insert(course);
        if (insert != 1){
            return Result.Failed("更新失败");
        }else {
            return Result.Ok("更新成功");

        }
    }

    /**
     * 更新
     */
    private Result update(Course course) {
        int i = courseMapper.updateById(course);
        if (i != 1){
            return Result.Failed("更新失败");
        }else {
            return Result.Ok("更新成功");

        }
    }

}

