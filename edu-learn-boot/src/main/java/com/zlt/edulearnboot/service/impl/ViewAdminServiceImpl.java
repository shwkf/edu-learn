package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.View;
import com.zlt.edulearnboot.entity.bo.ViewBo;
import com.zlt.edulearnboot.entity.vo.ViewVo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.CourseMapper;
import com.zlt.edulearnboot.mapper.ViewMapper;
import com.zlt.edulearnboot.service.ViewAdminService;
import com.zlt.edulearnboot.utils.UuidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class ViewAdminServiceImpl extends ServiceImpl<ViewMapper, View> implements ViewAdminService {
    @Autowired
    private ViewMapper viewMapper;
    @Autowired
    private CourseMapper courseMapper;
    /**
     * 删除面试题
     * @param id
     * @return
     */
    @Override
    public Result deleteView(String id){
        int i = viewMapper.deleteById(id);
        if (i!=1){
           return Result.Failed("删除失败");
        }else {
            return Result.Ok("删除成功");
        }
    }

    /**
     * 添加面试题
     * @param viewBo
     * @return
     */
    @Override
    public Result addView(ViewBo viewBo) {
        try {
            if (viewBo==null){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            View view = new View();
            System.out.println(viewBo);
            System.out.println(viewBo.getFileName());
            String fname = viewBo.getFileName();
            view.setId(UuidUtil.getShortUuid());
            view.setName(fname.split("\\.")[0]);
            view.setSuffix(fname.split("\\.")[1]);
            view.setSize(viewBo.getSize());
            view.setCourseId(viewBo.getCourseId());
            view.setPath(viewBo.getPath());
            int i = viewMapper.insert(view);
            if (i!=1){
                return Result.Failed("添加失败");
            }else {
                return Result.Ok("添加成功");
            }
        }catch (BaseException e){
            throw e;
        }
    }

    /**
     * 修改面试题
     * @param view
     * @return
     */
    @Override
    public Result updateView(View view) {
        int i = viewMapper.updateById(view);
        if (i!=1){
            return Result.Failed("更新失败");
        }else {
            return Result.Ok("更新成功");
        }
    }

    /**
     * 查询面试题
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result selectAllView(Integer pageNum, Integer pageSize) {
        Page<View> viewPage = viewMapper.selectPage(new Page<View>(pageNum, pageSize), null);
        //存放课程id
        ArrayList<String> courseIdList = new ArrayList<>();
        //结果集
        ArrayList<ViewVo> resultList = new ArrayList<>();
        if (!ObjectUtil.isEmpty(viewPage)){
            for (View record : viewPage.getRecords()) {
                String id = record.getCourseId();
                courseIdList.add(id);
            }
        }
        if (!courseIdList.isEmpty()){
            List<Course> courses = courseMapper.selectList(new QueryWrapper<Course>().in("id",courseIdList));
            HashMap<String,String> courseMap = new HashMap<>();
            for (Course cours : courses) {
                courseMap.put(cours.getId(),cours.getName());
            }
            System.out.println(courseMap);
            for (View record : viewPage.getRecords()) {
                ViewVo viewVo = new ViewVo();
                BeanUtils.copyProperties(record, viewVo);
                viewVo.setCourseName(courseMap.get(record.getCourseId()));
                resultList.add(viewVo);
            }
        }
        System.out.println(resultList);
        CommonPage<ViewVo> viewCommonPage = CommonPage.restPage(resultList, pageNum, pageSize, (int) viewPage.getPages(), viewPage.getTotal());
        return Result.Ok(viewCommonPage,"查询成功");
    }

    /**
     * 根据id查询面试题
     * @param id
     * @return
     */
    @Override
    public Result selectOneView(String id) {
        View view = viewMapper.selectById(id);
        if (view!=null){
            return Result.Ok(view);
        }else {
            return Result.Failed("数据错误");
        }
    }
}
