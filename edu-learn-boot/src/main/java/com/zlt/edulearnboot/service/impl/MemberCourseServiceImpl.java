package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Course;
import com.zlt.edulearnboot.entity.MemberCourse;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.CourseMapper;
import com.zlt.edulearnboot.mapper.MemberCourseMapper;
import com.zlt.edulearnboot.service.CourseService;
import com.zlt.edulearnboot.service.MemberCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlt.edulearnboot.utils.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 会员课程报名 服务实现类
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
@Service
public class MemberCourseServiceImpl extends ServiceImpl<MemberCourseMapper, MemberCourse> implements MemberCourseService {

    @Autowired
    private MemberCourseMapper memberCourseMapper;

    @Autowired
    private CourseMapper courseMapper;
    private MemberCourseService memberCourseService;


    @Override
    @Transactional
    public Result enrollCourse(String courseId,String memberId) {
        try {
            if (StrUtil.isBlank(courseId)&&StrUtil.isBlank(memberId)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            MemberCourse memberCourse = new MemberCourse();
            memberCourse.setId(UuidUtil.getShortUuid());
            memberCourse.setCourseId(courseId);
            memberCourse.setMemberId(memberId);
            List<Course> courses = courseMapper.selectList(new QueryWrapper<Course>().eq("id", courseId)
                    .eq("is_delete", 0));
            if(!courses.isEmpty()){//判断有无该课程
                MemberCourse mc = memberCourseMapper.selectOne(new QueryWrapper<MemberCourse>().eq("member_id", memberId).eq("course_id", courseId));
                if (ObjectUtil.isNull(mc)){//判断是否已经报过名了
                    if(memberCourseMapper.insert(memberCourse) > 0){
                        Integer enroll = countEnroll(courseId);
                        if(updateEnroll(courseId,enroll)) {
                            return Result.Ok("报名成功");
                        }else {
                            return Result.Failed("报名失败");
                        }
                    }else {
                        return Result.Failed("报名失败");
                    }
                }else {
                    return Result.Failed("已报名，请勿重复报名");
                }
            }else{
                return Result.Failed("没有该课程");
            }

        }catch (BaseException e){
            throw e;
        }

    }

    @Override
    public Result cancleCourse(String courseId, String memberId) {
        try {
            if (StrUtil.isBlank(courseId) && StrUtil.isBlank(memberId)) {
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            MemberCourse memberCourse = memberCourseMapper.selectOne(new QueryWrapper<MemberCourse>().eq("course_id", courseId)
                    .eq("member_id", memberId));
            memberCourse.setIsDelete(1);
            memberCourseMapper.update(memberCourse,new QueryWrapper<MemberCourse>().eq("course_id", courseId)
                    .eq("member_id", memberId));
            Integer enroll = countEnroll(courseId);
            if(updateEnroll(courseId,enroll)) {
                return Result.Ok("取消报名成功");
            }else {
                return Result.Failed("取消报名失败");
            }
        }catch(BaseException e){
            throw e;
        }
    }

    @Override
    public Integer countEnroll(String courseid) {
        QueryWrapper<MemberCourse> queryWrapper = new QueryWrapper();
        queryWrapper.eq("course_id", courseid).eq("is_delete", 0);
        Integer count = memberCourseMapper.selectCount(queryWrapper);
        return count;
    }

    @Override
    public boolean updateEnroll(String courseid,Integer enroll) {
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.set("enroll", enroll);
        int update = courseMapper.update(null, updateWrapper);
        return update>0;
    }

    @Override
    public CommonPage<Course> selectCourses(String id, Integer pageNum, Integer pageSize) {
        List<MemberCourse> memberCourses = memberCourseMapper.selectList(new QueryWrapper<MemberCourse>().eq("member_id", id));
        ArrayList list = new ArrayList();
        for (MemberCourse memberCours : memberCourses) {
            System.out.println(memberCours);
            list.add(memberCours.getCourseId());
        }
        Page<Course> coursePage = courseMapper.selectPage(new Page<Course>(pageNum,pageSize),new QueryWrapper<Course>().in("id",list));
        ArrayList<Course> courseArrayList = new ArrayList<>();
        for (Course record : coursePage.getRecords()) {
            courseArrayList.add(record);
        }
        return CommonPage.restPage(
                courseArrayList,
                pageNum,
                pageSize,
                (int)coursePage.getPages(),
                coursePage.getTotal()
        );
    }
}
