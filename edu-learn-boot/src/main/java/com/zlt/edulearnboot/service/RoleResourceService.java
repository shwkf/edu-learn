package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.RoleResource;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色资源关联 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface RoleResourceService extends IService<RoleResource> {

}
