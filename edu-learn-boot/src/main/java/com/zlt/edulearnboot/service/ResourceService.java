package com.zlt.edulearnboot.service;

import com.zlt.edulearnboot.entity.Resource;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资源 服务类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface ResourceService extends IService<Resource> {

}
