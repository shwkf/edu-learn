package com.zlt.edulearnboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Homework;
import com.zlt.edulearnboot.entity.Teacher;
import com.zlt.edulearnboot.entity.bo.HomeworkBo;

import java.util.Map;

public interface HomeworkAdminService extends IService<Homework> {
    Result deleteHomework(String id);

    Result addHomework(HomeworkBo homeworkBo, Map<String,String> map);

    Result updateHomework(Homework homework);

    CommonPage<Homework> selectAllHomework(Integer pageNum, Integer pageSize);

    Result selectHomework(String id);
}
