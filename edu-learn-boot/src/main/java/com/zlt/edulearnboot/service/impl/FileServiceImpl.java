package com.zlt.edulearnboot.service.impl;

import com.zlt.edulearnboot.entity.File;
import com.zlt.edulearnboot.mapper.FileMapper;
import com.zlt.edulearnboot.service.FileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文件 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, File> implements FileService {

}
