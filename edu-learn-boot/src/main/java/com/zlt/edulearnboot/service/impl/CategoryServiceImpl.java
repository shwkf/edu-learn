package com.zlt.edulearnboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlt.edulearnboot.entity.Category;
import com.zlt.edulearnboot.mapper.CategoryMapper;
import com.zlt.edulearnboot.service.CategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 分类 服务实现类
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public List<Category> sellectAll() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.ne("is_delete", 1);
        List<Category> categories = categoryMapper.selectList(queryWrapper);
        return categories;
    }
}
