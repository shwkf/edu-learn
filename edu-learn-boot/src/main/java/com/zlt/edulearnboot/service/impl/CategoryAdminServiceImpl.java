package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Category;
import com.zlt.edulearnboot.entity.bo.CategoryBo;
import com.zlt.edulearnboot.entity.vo.FirstLevelVo;
import com.zlt.edulearnboot.entity.vo.SecondLevelVo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.CategoryMapper;
import com.zlt.edulearnboot.service.CategoryAdminService;
import java.util.ArrayList;
import java.util.List;

import com.zlt.edulearnboot.utils.UuidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryAdminServiceImpl implements CategoryAdminService {
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public List<FirstLevelVo> getFirstAndSecondCategories() {
        QueryWrapper<Category> firstWrapper = new QueryWrapper();
        firstWrapper.eq("parent", "00000000").ne("is_delete",1);
        List<Category> first = this.categoryMapper.selectList(firstWrapper);
        QueryWrapper<Category> secWrapper = new QueryWrapper();
        firstWrapper.ne("parent", "00000000").ne("is_delete",1);
        List<Category> sec = this.categoryMapper.selectList(secWrapper);
        List<FirstLevelVo> finalList = new ArrayList();

        for(int i = 0; i < first.size(); ++i) {
            Category firstLevel = (Category)first.get(i);
            FirstLevelVo firstCategory = new FirstLevelVo();
            BeanUtils.copyProperties(firstLevel, firstCategory);
            List<SecondLevelVo> secFinalList = new ArrayList();

            for(int j = 0; j < sec.size(); ++j) {
                Category secLevel = (Category)sec.get(j);
                if (secLevel.getParent().equals(firstLevel.getId())) {
                    SecondLevelVo secCategory = new SecondLevelVo();
                    BeanUtils.copyProperties(secLevel, secCategory);
                    secFinalList.add(secCategory);
                }
            }

            firstCategory.setChildren(secFinalList);
            finalList.add(firstCategory);
        }

        return finalList;
    }

    @Override
    @Transactional
    public Result deleteCategory(String categoryId) {
        try {
            if (StrUtil.isBlank(categoryId)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            Category c = categoryMapper.selectById(categoryId);
            if (ObjectUtil.isNull(c)){
                throw new BaseException("分类不存在");
            }
            //判断是一级分类还是二级分类
            Category category = categoryMapper.selectById(categoryId);
            if (category.getParent().equals("00000000")){
                //是一级分类，将其子分类一起删除
                QueryWrapper<Category> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("parent",categoryId);
                int falg = categoryMapper.delete(queryWrapper);
                int flag2 = categoryMapper.deleteById(categoryId);
                if (falg >= 0 && flag2 > 0){
                    return Result.Ok("删除一级分类成功");
                }else {
                    return Result.Failed("删除一级分类失败");
                }

            }else {
                //是二级分类，删除本身即可
                int flag = categoryMapper.deleteById(categoryId);
                if (flag > 0){
                    return Result.Ok("删除二级分类成功");
                }else {
                    return Result.Failed("删除二级分类失败");
                }
            }
        }catch (BaseException e){
            throw e;
        }
    }

    @Override
    @Transactional
    public Result addFirCategory(CategoryBo categoryBo) {
        Category category = new Category();
        try {
            if (ObjectUtil.isNull(categoryBo)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            BeanUtil.copyProperties(categoryBo,category);
            System.out.println("xxxxxx");
            System.out.println(categoryBo);
            category.setId(UuidUtil.getShortUuid());
            category.setParent("00000000");
            int insert = categoryMapper.insert(category);
            if (insert != 1) {
                return Result.Failed("新增失败");
            }else {
                return Result.Ok("新增成功");
            }
        }catch (BaseException e){
            throw e;
        }
    }

    @Override
    @Transactional
    public Result addSecCategory(CategoryBo categoryBo) {
        Category category = new Category();
        try {
            if (ObjectUtil.isNull(categoryBo)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            BeanUtil.copyProperties(categoryBo,category);
            //name parent
            //判断parent是否存在
            Category parent = categoryMapper.selectById(category.getParent());
            if (ObjectUtil.isNull(parent)){
                return Result.Failed("添加二级分类失败，一级分类不存在");
            }else {
                category.setId(UuidUtil.getShortUuid());
                int insert = categoryMapper.insert(category);
                if (insert != 1) {
                    return Result.Failed("新增失败");
                }else {
                    return Result.Ok("新增成功");
                }
            }
        }catch (BaseException e){
            throw  e;
        }
    }


    @Override
    public Result selectFirstLevel() {
        List<Category> categories = categoryMapper.selectList(new QueryWrapper<Category>()
                .eq("parent", "00000000")
                .ne("is_delete", 1));
        return Result.Ok(categories,"查询一级分类成功");
    }

    @Override
    public Result selectSecondLevel() {
        List<Category> category = categoryMapper.selectList(new QueryWrapper<Category>().ne("parent", "00000000").eq("is_delete", 0));
        return Result.Ok(category,"查询二级分类成功");
    }
}
