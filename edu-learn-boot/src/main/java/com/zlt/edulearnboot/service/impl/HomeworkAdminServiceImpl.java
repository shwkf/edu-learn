package com.zlt.edulearnboot.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlt.edulearnboot.dto.CommonPage;
import com.zlt.edulearnboot.dto.Result;
import com.zlt.edulearnboot.entity.Homework;
import com.zlt.edulearnboot.entity.bo.HomeworkBo;
import com.zlt.edulearnboot.enums.ResultCode;
import com.zlt.edulearnboot.exception.BaseException;
import com.zlt.edulearnboot.mapper.HomeworkMapper;
import com.zlt.edulearnboot.service.HomeworkAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;
@Service
public class HomeworkAdminServiceImpl extends ServiceImpl<HomeworkMapper, Homework> implements HomeworkAdminService {
    @Autowired
    private HomeworkMapper homeworkMapper;

    /**
     * 删除作业
     * @param id
     * @return
     */
    @Override
    public Result deleteHomework(String id) {
        try {
            Homework homework = homeworkMapper.selectOne(new QueryWrapper<Homework>().eq("id",id).eq("is_delete", "0"));
            if (ObjectUtil.isNull(homework)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            int i = homeworkMapper.deleteById(id);
            if (i!=1){
                return Result.Failed("删除失败");
            }else{
                return Result.Ok("删除成功");
            }
        }catch (BaseException e){
            throw e;
        }
    }

    /**
     * 添加作业
     * @param homeworkBo
     * @return
     */
    @Override
    public Result addHomework(HomeworkBo homeworkBo, Map<String,String> map) {
        Homework homework = new Homework();
        String name = homeworkBo.getName();
        homework.setName(name.split("\\.")[0]);
        homework.setSuffix(name.split("\\.")[1]);
        homework.setSize(homeworkBo.getSize());
        homework.setSectionId(homework.getSectionId());
        homework.setPath("http://qvsyj51eh.hn-bkt.clouddn.com/"+map.get("key"));
        int insert = homeworkMapper.insert(homework);
        if (insert != 1){
            return  Result.Failed("添加失败");
        }else {
            return Result.Ok("添加成功");
        }
    }

    /**
     * 更新作业
     * @param homework
     * @return
     */
    @Override
    public Result updateHomework(Homework homework) {
        try {
            Homework homework1 = homeworkMapper.selectOne(new QueryWrapper<Homework>().eq("id", homework.getId()).eq("is_delete", 0));
            if(ObjectUtil.isNull(homework1)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            int i = homeworkMapper.updateById(homework);
            if (i!=1){
                return  Result.Failed("更新失败");
            }else {
                return Result.Ok("更新成功");
            }
        }catch (BaseException e){
            throw  e;
        }
    }

    /**
     * 查询所有作业
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public CommonPage<Homework> selectAllHomework(Integer pageNum, Integer pageSize) {
        try {
            Page<Homework> homeworkPage = homeworkMapper.selectPage(new Page<Homework>(pageNum, pageSize), new QueryWrapper<Homework>().eq("is_delete", "0"));
            if (ObjectUtil.isNull(homeworkPage)){
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            ArrayList<Homework> list = new ArrayList<>();
            for (Homework record : homeworkPage.getRecords()) {
                list.add(record);
            }
            return CommonPage.restPage(list, pageNum, pageSize, (int) homeworkPage.getPages(),homeworkPage.getTotal());

        }catch (BaseException e){
            throw e;
        }
       }

    /**
     * 根据id查询作业
     * @param id
     * @return
     */
    @Override
    public Result selectHomework(String id) {
        try {
            Homework homework = homeworkMapper.selectOne(new QueryWrapper<Homework>().eq("id", id).eq("is_delete", "0"));
            if (ObjectUtil.isNull(homework)) {
                throw new BaseException(ResultCode.VALIDATE_FAILED);
            }
            return Result.Ok(homework,"查询成功");
        }catch (BaseException e){
            throw e;
        }
    }
}
