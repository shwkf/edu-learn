package com.zlt.edulearnboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@EnableOpenApi
@SpringBootApplication
@MapperScan("com.zlt.edulearnboot.mapper")
public class EduLearnBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(EduLearnBootApplication.class, args);
    }

}
