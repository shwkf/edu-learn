package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.Chapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 大章 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface ChapterMapper extends BaseMapper<Chapter> {

}
