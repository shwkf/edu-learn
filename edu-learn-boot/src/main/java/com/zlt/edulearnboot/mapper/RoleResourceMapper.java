package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.RoleResource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色资源关联 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface RoleResourceMapper extends BaseMapper<RoleResource> {

}
