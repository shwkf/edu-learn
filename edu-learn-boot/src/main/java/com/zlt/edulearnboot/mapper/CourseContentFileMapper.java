package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.CourseContentFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程内容文件 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface CourseContentFileMapper extends BaseMapper<CourseContentFile> {

}
