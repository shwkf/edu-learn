package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.RoleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色用户关联 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface RoleUserMapper extends BaseMapper<RoleUser> {

}
