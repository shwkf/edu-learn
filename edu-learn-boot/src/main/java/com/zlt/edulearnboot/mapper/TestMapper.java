package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.Test;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 测试 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface TestMapper extends BaseMapper<Test> {

}
