package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.CourseContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程内容 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface CourseContentMapper extends BaseMapper<CourseContent> {

}
