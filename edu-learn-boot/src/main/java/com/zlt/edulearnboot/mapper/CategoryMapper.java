package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分类 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
