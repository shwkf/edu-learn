package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface MemberMapper extends BaseMapper<Member> {

}
