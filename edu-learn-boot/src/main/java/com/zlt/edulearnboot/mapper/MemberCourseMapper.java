package com.zlt.edulearnboot.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlt.edulearnboot.entity.MemberCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 会员课程报名 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
public interface MemberCourseMapper extends BaseMapper<MemberCourse> {

    List<MemberCourse> selectList(QueryWrapper<Object> member_id);
}
