package com.zlt.edulearnboot.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlt.edulearnboot.entity.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface TeacherMapper extends BaseMapper<Teacher> {

}
