package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.Sms;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 短信验证码 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface SmsMapper extends BaseMapper<Sms> {

}
