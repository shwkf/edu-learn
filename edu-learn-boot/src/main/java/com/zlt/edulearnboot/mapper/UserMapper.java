package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface UserMapper extends BaseMapper<User> {

}
