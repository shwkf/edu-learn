package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.File;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface FileMapper extends BaseMapper<File> {

}
