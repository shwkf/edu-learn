package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.CourseCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程分类 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface CourseCategoryMapper extends BaseMapper<CourseCategory> {

}
