package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.Homework;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
public interface HomeworkMapper extends BaseMapper<Homework> {

}
