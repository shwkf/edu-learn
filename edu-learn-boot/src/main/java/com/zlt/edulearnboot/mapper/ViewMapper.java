package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.View;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface ViewMapper extends BaseMapper<View> {

}
