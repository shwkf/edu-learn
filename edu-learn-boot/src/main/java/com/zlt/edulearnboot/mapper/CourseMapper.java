package com.zlt.edulearnboot.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zlt.edulearnboot.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zlt.edulearnboot.entity.vo.CourseReportVo;

import java.util.List;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-07-09
 */
public interface CourseMapper extends BaseMapper<Course> {

    Object selectList(QueryWrapper<Object> id);

    List<CourseReportVo> selectReport(String year, String month);
}
