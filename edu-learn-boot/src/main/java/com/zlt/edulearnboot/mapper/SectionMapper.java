package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.Section;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 小节 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface SectionMapper extends BaseMapper<Section> {

}
