package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.Resource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 资源 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface ResourceMapper extends BaseMapper<Resource> {

}
