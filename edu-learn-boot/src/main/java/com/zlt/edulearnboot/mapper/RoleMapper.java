package com.zlt.edulearnboot.mapper;

import com.zlt.edulearnboot.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-07-09
 */
public interface RoleMapper extends BaseMapper<Role> {

}
